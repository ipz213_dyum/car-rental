<?php

namespace core;

class Utils
{
    public static function filterArray($array, $fieldsList): array
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            if (in_array($key, $fieldsList)) {
                $newArray[$key] = $value;
            }
        }
        return $newArray;
    }

    public static function sortAsc($elements, $field)
    {
        $size = count($elements) - 1;
        $elements = array_combine(range(0, $size), $elements);
        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size - $i; $j++) {
                $k = $j + 1;
                if (floatval($elements[$k][$field]) < floatval($elements[$j][$field])) {

                    list($elements[$j], $elements[$k]) = array($elements[$k], $elements[$j]);
                }
            }
        }
        return $elements;
    }

    public static function sortDesc($elements, $field)
    {
        $size = count($elements) - 1;
        $elements = array_combine(range(0, $size), $elements);
        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size - $i; $j++) {
                $k = $j + 1;
                if (floatval($elements[$k][$field]) > floatval($elements[$j][$field])) {
                    list($elements[$j], $elements[$k]) = array($elements[$k], $elements[$j]);
                }
            }
        }
        return $elements;
    }

    public static function filterByFieldInArray($elements, $field, $valuesArray)
    {
        foreach ($elements as $k => $element) {
            if (!in_array($element[$field], $valuesArray)) {
                unset($elements[$k]);
            }
        }
        return $elements;
    }

    public static function filterByFieldContainsArrayElement($elements, $field, $valuesArray)
    {
        foreach ($elements as $k => $element) {
            foreach ($valuesArray as $value) {
                if (strpos(strtolower($element[$field]), $value) === false) {
                    unset($elements[$k]);
                }
            }
        }
        return $elements;
    }

    public static function filterArrayByMaxAndMin($elements, $field, $min = null, $max = null)
    {
        foreach ($elements as $k => $element) {
            if (!empty($min) && $element[$field] < $min) {
                unset($elements[$k]);
            } else if (!empty($max) && $element[$field] > $max) {
                unset($elements[$k]);
            }
        }
        return $elements;
    }
}