<?php

namespace core;
/**
 * Class for executing queries to DB
 */
class DB
{
    protected \PDO $pdo;

    public function __construct($hostname, $login, $password, $database)
    {
        $this->pdo = new \PDO("mysql: host={$hostname};dbname={$database}", $login, $password);
    }

    /**
     * Execution of a request to receive data from the specified database table
     * @param string $tableName DB Table name
     * @param string|array $fieldsList List of fields for select
     * @param null|array $conditionArray Assoc array of WHERE condition
     * @return array|false
     */
    public function select(string $tableName, $fieldsList = '*', array $conditionArray = null,
                           array  $orderBy = null, string $orderType = 'asc')
    {
        if (is_string($fieldsList))
            $fieldsListString = $fieldsList;
        if (is_array($fieldsList))
            $fieldsListString = implode(', ', $fieldsList);
        $wherePart = "";
        if (is_array($conditionArray)) {
            $parts = [];
            foreach ($conditionArray as $key => $value) {
                $parts[] = "{$key} = :{$key}";
            }
            $wherePart = "WHERE " . implode(' AND ', $parts);
        }
        $orderByPart = "";
        if (is_array($orderBy)) {
            $orderByPart = "ORDER BY" . implode(', ', $orderBy) . " " . $orderType;
        }

        $res = $this->pdo->prepare("SELECT {$fieldsListString} FROM {$tableName} {$wherePart} {$orderByPart}");

        $res->execute($conditionArray);
        return $res->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function update($tableName, $newValuesArray, $conditionArray)
    {
        $setParts = [];
        $paramsArray = [];
        foreach ($newValuesArray as $key => $value) {
            $setParts[] = "{$key} = :set{$key}";
            $paramsArray['set' . $key] = $value;
        }

        $whereParts = [];
        foreach ($conditionArray as $key => $value) {
            $whereParts[] = "$key = :$key";
            $paramsArray[$key] = $value;
        }
        $wherePart = "WHERE " . implode(' AND ', $whereParts);
        $setPartString = implode(', ', $setParts);
        $res = $this->pdo->prepare("UPDATE {$tableName} SET {$setPartString} {$wherePart}");
        return $res->execute($paramsArray);
    }

    public function insert($tableName, $newRowArray)
    {
        $fieldsArray = array_keys($newRowArray);
        $fieldsListString = implode(', ', $fieldsArray);
        $paramsArray = [];
        foreach ($newRowArray as $key => $value) {
            $paramsArray[] = ":" . $key;
        }
        $valuesListString = implode(', ', $paramsArray);
        $res = $this->pdo->prepare("INSERT INTO {$tableName} ($fieldsListString) VALUES($valuesListString)");
        $res->execute($newRowArray);
        return $this->pdo->lastInsertId();
    }

    public function delete($tableName, $conditionArray)
    {
        $whereParts = [];
        foreach ($conditionArray as $key => $value) {
            $whereParts[] = "$key = :$key";
        }
        $wherePart = "WHERE " . implode(' AND ', $whereParts);
        $res = $this->pdo->prepare("DELETE FROM {$tableName} {$wherePart}");
        return $res->execute($conditionArray);
    }

    public function query($query, $params)
    {
        $res = $this->pdo->prepare($query);
        $res->execute($params);
        return $res->fetchAll(\PDO::FETCH_ASSOC);
    }
}