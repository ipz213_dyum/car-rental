<?php

namespace core;

use http\Params;

class Controller
{
    protected string $viewPath;
    protected string $moduleName;
    protected string $actionName;

    public function __construct()
    {
        if (!\core\Core::getInstance()->isAjax) {
            $this->moduleName = \core\Core::getInstance()->app['moduleName'];
            $this->actionName = \core\Core::getInstance()->app['actionName'];
            $this->viewPath = "views/{$this->moduleName}/{$this->actionName}.php";
        }
    }

    public function render($viewPath = null, $params = null)
    {
        if ($viewPath == null)
            $viewPath = $this->viewPath;
        $tpl = new Template($viewPath);
        if ($params != null)
            $tpl->setParams($params);
        return $tpl->getHTML();
    }

    public function renderView($viewName, $params = null)
    {
        $path = "views/{$this->moduleName}/{$viewName}.php";
        return $this->render($path, $params);
    }

    public function redirect($url)
    {
        header("Location: {$url}");
        die;
    }

    public function error($error, $message = null)
    {
        return new Error($error, $message);
    }
}