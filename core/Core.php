<?php

namespace core;

use controllers\MainController;

class Core
{
    private static Core $instance;
    public array $app;
    public array $pageParams;
    public DB $db;
    public string $requestMethod;

    private function __construct()
    {
        global $pageParams;
        $this->app = [];
        $this->pageParams = $pageParams;
        $this->isAjax = false;
    }

    public static function getInstance(): Core
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function Initialize()
    {
        session_start();
        $this->db = new DB(DATABASE_HOST, DATABASE_LOGIN, DATABASE_PASSWORD,
            DATABASE_BASENAME);
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }

    public function Run()
    {
        $route = $_GET['route'];
        $routeParts = explode('/', $route);
        if ($routeParts[0] == 'ajax') {
            $this->isAjax=true;
            $moduleName = $routeParts[1];
            $actionName = $routeParts[2];
            if ($moduleName != null && $actionName != null) {
                $moduleName = '\\controllers\\' . ucfirst($moduleName) . 'Controller';
                $actionName = $actionName . 'AjaxAction';
                $moduleName = new $moduleName();
                $moduleName->$actionName();
                die();
            }
        }
        $moduleName = strtolower(array_shift($routeParts));
        $actionName = strtolower(array_shift($routeParts));
        if (empty($moduleName))
            $moduleName = 'main';
        if (empty($actionName))
            $actionName = "index";
        $this->app['moduleName'] = $moduleName;
        $this->app['actionName'] = $actionName;
        $controllerName = '\\controllers\\' . ucfirst($moduleName) . 'Controller';
        $controllerActionName = $actionName . 'Action';
        $statusCode = 200;
        if (class_exists($controllerName)) {
            $controller = new $controllerName();
            if (method_exists($controller, $controllerActionName)) {
                $actionResult = $controller->$controllerActionName($routeParts);
                if ($actionResult instanceof Error)
                    $statusCode = $actionResult->code;
                $this->pageParams['content'] = $actionResult;
            } else {
                $statusCode = 404;
            }
        } else {
            $statusCode = 404;
        }
        $statusCodeType = intval($statusCode / 100);
        if ($statusCodeType == 4 || $statusCodeType == 5) {
            $mainController = new MainController();
            $this->pageParams['content'] = $mainController->errorAction($statusCode);
        }

    }

    public function Done()
    {
        if (!$this->isAjax) {
            $pathToLayout = 'themes/light/layout.php';
            $tpl = new Template($pathToLayout);
            $tpl->setParams($this->pageParams);
            $html = $tpl->getHTML();
            echo $html;
        }
    }
}