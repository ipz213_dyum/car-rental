<?php

namespace models;


use core\Core;
use core\Utils;

class User
{

    protected static string $tableName = 'user';

    public static function addUser($login, $password, $firstName, $lastName, $phoneNumber)
    {
        \core\Core::getInstance()->db->insert(self::$tableName, [
            'login' => $login,
            'password' => self::hashPassword($password),
            'firstName' => $firstName,
            'lastName' => $lastName,
            'phone_number' => $phoneNumber,
        ]);
    }

    public static function hashPassword(string $password): string
    {
        return md5($password);
    }

    public static function updateUser($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['lastname', 'firstname', 'login', 'phone_number']);
        \core\Core::getInstance()->db->update(self::$tableName, $updatesArray, ['id' => $id]);
        if ($id == self::getCurrentAuthenticatedUser()['id']) {
            foreach ($updatesArray as $key => $update) {
                $_SESSION['user'][$key] = $update;
            }
        }
    }

    public static function updatePassword($newPassword)
    {
        $id = User::getCurrentAuthenticatedUser()['id'];
        \core\Core::getInstance()->db->update(self::$tableName, [
            'password' => self::hashPassword($newPassword)
        ], [
            'id' => $id
        ]);
        $_SESSION['user']['password'] = self::hashPassword($newPassword);
    }

    public static function isLoginExists(string $login): bool
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login
        ]);
        return !empty($user);
    }

    public static function isPhoneNumberExists(string $phoneNumber): bool
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'phone_number' => $phoneNumber
        ]);
        return !empty($user);
    }

    public static function verifyUser($login, $password): bool
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => $password
        ]);
        return !empty($user);
    }

    public static function getUserByLoginAndPassword($login, $password)
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => self::hashPassword($password)
        ]);
        if (!empty($user))
            return $user[0];
        return null;
    }

    public static function authenticateUser($user)
    {
        $_SESSION['user'] = $user;
    }

    public static function logoutUser()
    {
        unset($_SESSION['user']);
    }

    public static function isUserAuthenticated(): bool
    {
        return isset($_SESSION['user']);
    }

    public static function getCurrentAuthenticatedUser()
    {
        return $_SESSION['user'];
    }

    public static function isAdmin(): bool
    {
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 10;
    }

    public static function isManager(): bool
    {
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 5;
    }

    public static function getUsers()
    {
        return Core::getInstance()->db->select(self::$tableName, ['id', 'login', 'firstname', 'lastname', 'access_level',
            'phone_number', 'access_level']);
    }

    public static function getAdmins()
    {
        return Core::getInstance()->db->select(self::$tableName, ['id', 'login', 'firstname', 'lastname', 'access_level',
            'phone_number', 'access_level'], ['access_level' => 10]);
    }

    public static function getManagers()
    {
        return Core::getInstance()->db->select(self::$tableName, ['id', 'login', 'firstname', 'lastname', 'access_level',
            'phone_number', 'access_level'], ['access_level' => 5]);
    }

    public static function giveAdminLevel($userId)
    {
        Core::getInstance()->db->update(self::$tableName, ['access_level' => 10], ['id' => $userId]);
    }
    public static function giveManagerLevel($userId)
    {
        Core::getInstance()->db->update(self::$tableName, ['access_level' => 5], ['id' => $userId]);
    }

    public static function removeAdminLevel($userId)
    {
        Core::getInstance()->db->update(self::$tableName, ['access_level' => 1], ['id' => $userId]);
    }
}