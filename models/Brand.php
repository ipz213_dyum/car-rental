<?php

namespace models;

use core\Core;

class Brand
{
    protected static string $tableName = 'brand';

    public static function isBrandById($id): bool
    {
        $rows = Core::getInstance()->db->select(self::$tableName, 'id', [
            'id' => $id
        ]);
        return count($rows) > 0;
    }

    public static function addBrand($name, $photoPath)
    {
        do {
            $fileName = uniqid() . ".png";
            $newPath = "files/brand/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($photoPath, $newPath);
        Core::getInstance()->db->insert(self::$tableName, ['name' => $name, 'photo' => $fileName]);
    }

    public static function getBrands()
    {
        return Core::getInstance()->db->select(self::$tableName);
    }

    public static function getBrandById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        if (!empty($rows))
            return $rows[0];
        return null;
    }

    public static function deletePhotoFile($id)
    {
        $row = self::getBrandById($id);
        $photoPath = 'files/brand/' . $row['photo'];
        if (is_file($photoPath))
            unlink($photoPath);
    }

    public static function changePhoto($id, $newPhoto)
    {
        self::deletePhotoFile($id);
        do {
            $fileName = uniqid() . ".jpg";
            $newPath = "files/brand/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($newPhoto, $newPath);
        Core::getInstance()->db->update(self::$tableName, ['photo' => $fileName], ['id' => $id]);
    }

    public static function deleteBrandById($id)
    {
        self::deletePhotoFile($id);
        Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
    }

    public static function updateBrand($id, $newName)
    {
        Core::getInstance()->db->update(self::$tableName, ['name' => $newName], ['id' => $id]);
    }
}