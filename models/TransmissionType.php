<?php

namespace models;

use core\Core;

class TransmissionType
{
    public static string $tableName = 'transmission';

    public static function getTransmissionTypes()
    {
        return Core::getInstance()->db->select(self::$tableName);
    }

    public static function addTransmissionType($name)
    {
        Core::getInstance()->db->insert(self::$tableName, ['name' => $name]);
    }

    public static function getTransmissionById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        return $row[0];
    }
}