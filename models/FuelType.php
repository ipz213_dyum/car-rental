<?php

namespace models;

use core\Core;

class FuelType
{
    public static string $tableName = 'fuel_type';

    public static function getFuelTypes()
    {
        return Core::getInstance()->db->select(self::$tableName);
    }

    public static function addFuelType($name)
    {
        Core::getInstance()->db->insert(self::$tableName, ['name' => $name]);
    }

    public static function getFuelTypeById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        return $row[0];
    }
}