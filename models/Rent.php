<?php

namespace models;

use core\Core;
use mysql_xdevapi\RowResult;

class Rent
{
    protected static string $tableName = 'rent';

    public static function isRentById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, 'id', ['id' => $id]);
        return count($rows) > 0;
    }

    public static function createRentRequest($carId, $startDate, $endDate, $delivery_location)
    {
        $rentId = Core::getInstance()->db->insert(self::$tableName, [
            'user_id' => User::getCurrentAuthenticatedUser()['id'],
            'car_id' => $carId,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'delivery_location' => $delivery_location,
        ]);
        return $rentId;
    }

    public static function getRents()
    {
        $rows = Core::getInstance()->db->query('SELECT cp.photo, c.model, c.city, DATEDIFF(r.end_date, r.start_date) as days_count,
       DATEDIFF(r.end_date, r.start_date)*c.price as total_price, u.firstname as firstname, u.lastname as lastname,
       u.phone_number as phone_number, r.* FROM rent r 
                join car c on r.car_id = c.id
                join user u on r.user_id = u.id
                join car_photo cp on c.main_photo_id = cp.id
                order by r.id desc', null);
        return $rows;
    }

    public static function getRequests()
    {
        $rows = Core::getInstance()->db->query('SELECT cp.photo, c.model, c.city, DATEDIFF(r.end_date, r.start_date) as days_count,
       DATEDIFF(r.end_date, r.start_date)*c.price as total_price, u.firstname as firstname, u.lastname as lastname,
       u.phone_number as phone_number, r.* FROM rent r 
                join car c on r.car_id = c.id
                join user u on r.user_id = u.id
                join car_photo cp on c.main_photo_id = cp.id
                where r.approved = 0
                order by r.id desc', null);
        return $rows;
    }

    public static function getActiveRents()
    {
        $rows = Core::getInstance()->db->query('SELECT cp.photo, c.model, c.city, DATEDIFF(r.end_date, r.start_date) as days_count,
       DATEDIFF(r.end_date, r.start_date)*c.price as total_price, u.firstname as firstname, u.lastname as lastname,
       u.phone_number as phone_number, r.* FROM rent r 
                join car c on r.car_id = c.id
                join user u on r.user_id = u.id
                join car_photo cp on c.main_photo_id = cp.id
                where  r.start_date<=CURDATE() and r.end_date >= CURDATE() and r.approved = 1
                order by r.id desc', null);
        return $rows;
    }

    public static function getEndedRents()
    {
        $rows = Core::getInstance()->db->query('SELECT cp.photo, c.model, c.city, DATEDIFF(r.end_date, r.start_date) as days_count,
       DATEDIFF(r.end_date, r.start_date)*c.price as total_price, u.firstname as firstname, u.lastname as lastname,
       u.phone_number as phone_number, r.* FROM rent r 
                join car c on r.car_id = c.id
                join user u on r.user_id = u.id
                join car_photo cp on c.main_photo_id = cp.id
                where r.end_date < CURDATE() and r.approved=1
                order by r.id desc', null);
        return $rows;
    }

    public static function approveRentRequest($id)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'approved' => 1
        ], [
            'id' => $id
        ]);
    }

    public static function declineRentRequest($id)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'approved' => 2
        ], [
            'id' => $id
        ]);
    }

    public static function getRentById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        return $rows[0];
    }

    public static function getCarRents($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'car_id' => $id
        ]);
    }

    public static function getUserCurrentCarRent($userId, $carId)
    {
        $rows = Core::getInstance()->db->query('SELECT * FROM rent
         WHERE user_id = :userId and car_id = :carId and end_date >= CURDATE()', [
            'carId' => $carId,
            'userId' => $userId
        ]);
        return $rows[0];
    }

    public static function cancelRentById($id)
    {
        Core::getInstance()->db->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function isCarRentedForDate($carId, $startDate, $endDate): bool
    {
        $rows = Core::getInstance()->db->query('SELECT * FROM rent
                WHERE ((:startDate<=start_date and :endDate >= start_date)
                or (:startDate<=end_date and :endDate>=end_date)
                or (:startDate>=start_date and :endDate<=end_date))
                and approved = 1
                and car_id = :carId'
            , [
                'carId' => $carId,
                'startDate' => $startDate,
                'endDate' => $endDate,
            ]);
        return count($rows) > 0;
    }
    public static function getRentsInRange($startDate, $endDate){
        return Core::getInstance()->db->query(
            "select r.*, b.name as brand, c.model, c.price, u.firstname, u.lastname,
        DATEDIFF(r.end_date, r.start_date) as rentDays,
        c.price*DATEDIFF(r.end_date, r.start_date) as totalAmount
        from rent r
        join car c on r.car_id = c.id 
        join brand b on c.brand_id = b.id 
        join user u on r.user_id = u.id 
        where r.start_date >= :startDate and r.start_date < :endDate and r.approved=1", [
            'startDate' => $startDate,
            'endDate' => $endDate
        ]);
    }
    public static function getUserRents($userId)
    {
        $rows = Core::getInstance()->db->query('SELECT c.model, c.price, c.city, cp.photo, 
       DATEDIFF(r.end_date, r.start_date) as days_count,
       DATEDIFF(r.end_date, r.start_date)*c.price as total_price, r.* FROM rent r 
             join car c on r.car_id = c.id
             join car_photo cp on c.main_photo_id = cp.id
             where r.user_id = :user_id
             order by r.id desc',
            [
                'user_id' => $userId
            ]);
        return $rows;
    }
}