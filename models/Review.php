<?php

namespace models;

use core\Core;

class Review
{
    protected static string $tableName = 'review';

    public static function addReview($car_id, $user_id, $text, $rating)
    {
        $id = null;
        $userReview = Core::getInstance()->db->select(self::$tableName, '*', [
            'car_id' => $car_id,
            'user_id' => $user_id,
        ]);
        if (count($userReview) > 0) {
            $id = $userReview['id'];
            Core::getInstance()->db->update(self::$tableName, [
                'text' => $text,
                'rating' => $rating,
                'date' => date('Y-m-d'),
            ], [
                'car_id' => $car_id,
                'user_id' => $user_id,
            ]);
        } else {
            $id = Core::getInstance()->db->insert(self::$tableName, [
                'car_id' => $car_id,
                'user_id' => $user_id,
                'text' => $text,
                'rating' => $rating,
                'date' => date('Y-m-d'),
            ]);
        }
        return $id;
    }

    public static function getReviewsByCarId($id)
    {
        return Core::getInstance()->db->query("select r.*, u.firstname, u.lastname from review r
        join user u on r.user_id = u.id where r.car_id = :id
        ORDER BY r.id DESC", [
            'id' => $id
        ]);
    }

    public static function getGoodReviews()
    {
        return Core::getInstance()->db->query("select r.*, u.firstname, u.lastname, c.model from review r
        join user u on r.user_id = u.id
        join car c on r.car_id = c.id
        where rating = :rating and rTRIM(r.text)<>''
        order by r.id desc ", [
            'rating' => 5
        ]);
    }

    public static function getReviewById($id)
    {
        $rows = Core::getInstance()->db->select('review', '*', [
            'id' => $id,
        ]);
        return $rows[0];
    }

    public static function deleteReviewById($id)
    {
        Core::getInstance()->db->delete('review', [
            'id' => $id,
        ]);
    }

    public static function isReviewById($id): bool
    {
        $rows = Core::getInstance()->db->select(self::$tableName, ['id'], [
            'id' => $id
        ]);
        return count($rows) > 0;
    }
}