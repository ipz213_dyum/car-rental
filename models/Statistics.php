<?php

namespace models;

use core\Core;

class Statistics
{
    public static function getStatistics($startDate, $endDate)
    {
        return Core::getInstance()->db->query(
            "select count(*) as rentsCount,
        sum(c.price*datediff(r.end_date, r.start_date)) as moneyEarned,
        ROUND(AVG(datediff(r.end_date, r.start_date)),1) as avgRentPeriod
        from rent r
        join car c on r.car_id = c.id 
        where r.start_date >= :startDate and r.start_date <= :endDate and r.approved=1", [
            'startDate' => $startDate,
            'endDate' => $endDate
        ])[0];
    }

    public static function getRentStatisticForYear($year)
    {
        return Core::getInstance()->db->query(
            "select month(start_date) as month, COUNT(*) as rentsCount
            from rent
            where year(start_date) = :year and approved = 1
            group by month(start_date)", [
            'year' => $year,
        ]);
    }

    public static function getRentStatisticForMonth($year, $month)
    {
        return Core::getInstance()->db->query(
            "select day(start_date) as day, COUNT(*) as rentsCount
            from rent
            where year(start_date) = :year and month(start_date) = :month and approved = 1
            group by day(start_date)", [
            'year' => $year,
            'month' => $month
        ]);
    }

    public static function getRentCategoryStatisticForMonth($year, $month)
    {
        return Core::getInstance()->db->query(
            "SELECT cat.name as category, COUNT(*) as rentsCount FROM rent r 
join car c on r.car_id = c.id
join category cat on c.category_id = cat.id
where month(r.start_date) = :month and year(r.start_date) = :year
group by cat.name", [
            'year' => $year,
            'month' => $month
        ]);
    }

    public static function getRentCategoryStatisticForYear($year)
    {
        return Core::getInstance()->db->query(
            "SELECT cat.name as category, COUNT(*) as rentsCount FROM rent r 
join car c on r.car_id = c.id
join category cat on c.category_id = cat.id
where year(r.start_date) = :year
group by cat.name", [
            'year' => $year,
        ]);
    }

    public static function getRentBrandsStatisticForMonth($year, $month)
    {
        return Core::getInstance()->db->query(
            "SELECT b.name as brand, COUNT(*) as rentsCount FROM rent r 
join car c on r.car_id = c.id
join brand b on c.brand_id = b.id
where month(r.start_date) = :month and year(r.start_date) = :year
group by b.name", [
            'year' => $year,
            'month' => $month
        ]);
    }

    public static function getRentBrandsStatisticForYear($year)
    {
        return Core::getInstance()->db->query(
            "SELECT b.name as brand, COUNT(*) as rentsCount FROM rent r 
join car c on r.car_id = c.id
join brand b on c.brand_id = b.id
where year(r.start_date) = :year
group by b.name", [
            'year' => $year,
        ]);
    }
}