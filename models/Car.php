<?php

namespace models;

use core\Core;
use core\Utils;

class Car
{
    protected static string $tableName = 'car';
    protected static array $fieldsList = ['model',
        'price',
        'kilometrage',
        'transmission_id',
        'fuel_type_id',
        'fuel_consumption',
        'seats',
        'city',
        'description',
        'visible',
        'brand_id',
        'category_id',
        'main_photo_id'
    ];

    public static function isCarById($id): bool
    {
        $rows = Core::getInstance()->db->select(self::$tableName, 'id', [
            'id' => $id
        ]);
        return count($rows) > 0;
    }

    public static function addCar($model, $price, $kilometrage, $transmission_id, $fuel_type_id, $fuel_consumption, $seats,
                                  $city, $description, $visible, $brand_id, $category_id): int
    {
        $car_id = Core::getInstance()->db->insert(self::$tableName, [
            'model' => $model,
            'price' => $price,
            'kilometrage' => $kilometrage,
            'transmission_id' => $transmission_id,
            'fuel_type_id' => $fuel_type_id,
            'fuel_consumption' => $fuel_consumption,
            'seats' => $seats,
            'city' => $city,
            'description' => $description,
            'visible' => $visible,
            'brand_id' => $brand_id,
            'category_id' => $category_id
        ]);
        return $car_id;
    }

    public static function updateCar($id, $params)
    {
        $params = Utils::filterArray($params, self::$fieldsList);
        $res = Core::getInstance()->db->update(self::$tableName, $params, ['id' => $id]);
    }

    public static function deleteCarById($id)
    {
        $carPhotos = self::getCarPhotosById($id);
        foreach ($carPhotos as $photos) {
            $filePath = "files/car/" . $photos['photo'];
            if (is_file($filePath))
                unlink($filePath);
        }
        Core::getInstance()->db->delete('car_photo', ['car_id' => $id]);
        Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
    }

    public static function addPhotos($carId, $photosArray, $mainPhotoIndex = null)
    {
        $mainId = null;
        for ($i = 0; $i < count($photosArray); $i++) {
            do {
                $fileName = uniqid() . ".jpg";
                $newPath = "files/car/{$fileName}";
            } while (file_exists($newPath));
            var_dump($newPath);
            move_uploaded_file($photosArray[$i], $newPath);
            $id = Core::getInstance()->db->insert('car_photo', [
                'car_id' => $carId,
                'photo' => $fileName
            ]);
            if ($mainPhotoIndex != null && $i == $mainPhotoIndex) {
                $mainId = $id;
            }
        }
        return $mainId;
    }

    public static function deletePhotoById($id)
    {
        $photo = Core::getInstance()->db->select('car_photo', '*', ['id' => $id]);
        $filePath = "files/car/" . $photo[0]['photo'];
        if (is_file($filePath))
            unlink($filePath);
        $photo = Core::getInstance()->db->delete('car_photo', ['id' => $id]);
    }

    public static function setMainPhotoForCar($car_id, $photo_id)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'main_photo_id' => $photo_id
        ], ['id' => $car_id]);
    }

    public static function getCarPhotosById($car_id)
    {
        return Core::getInstance()->db->select('car_photo', '*', [
            'car_id' => $car_id
        ]);
    }

    public static function getCarById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        return $row[0];
    }

    public static function getTripsCountById($id): int
    {
        $rows = Core::getInstance()->db->select('rent', '*', [
            'car_id' => $id,
            'approved' => 1
        ]);
        return count($rows);
    }

    public static function getCars($categories, $brands)
    {
        $rows = Core::getInstance()->db->query(
            "SELECT c.*,cp.photo, ROUND(avg(r.rating),1) as avg_rating,
                    CONCAT(UCASE(LEFT(city, 1)),LCASE(SUBSTRING(city, 2))) as city from car c
					left join car_photo cp on c.main_photo_id = cp.id
                    left join review r on c.id = r.car_id
                    group by c.id, cp.photo", null);
        return $rows;
    }

    public static function getCities()
    {
        return Core::getInstance()->db->query('SELECT CONCAT(UCASE(LEFT(city, 1)),LCASE(SUBSTRING(city, 2))) as name FROM car
group by name', null);

    }
}