<?php

namespace models;

use core\Core;

class Category
{
    protected static string $tableName = 'category';

    public static function isCategoryById($id): bool
    {
        $rows = Core::getInstance()->db->select(self::$tableName, 'id', [
            'id' => $id
        ]);
        return count($rows) > 0;
    }

    public static function addCategory($categoryName, $photoPath)
    {
        do {
            $fileName = uniqid() . ".jpg";
            $newPath = "files/category/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($photoPath, $newPath);
        Core::getInstance()->db->insert(self::$tableName, ['name' => $categoryName, 'photo' => $fileName]);
    }

    public static function getCategoryById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        if (!empty($rows))
            return $rows[0];
        return null;
    }

    public static function deletePhotoFile($id)
    {
        $row = self::getCategoryById($id);
        $photoPath = 'files/category/' . $row['photo'];
        if (is_file($photoPath))
            unlink($photoPath);
    }

    public static function changePhoto($id, $newPhoto)
    {
        self::deletePhotoFile($id);
        do {
            $fileName = uniqid() . ".jpg";
            $newPath = "files/category/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($newPhoto, $newPath);
        Core::getInstance()->db->update(self::$tableName, ['photo' => $fileName], ['id' => $id]);
    }

    public static function deleteCategoryById($id)
    {
        self::deletePhotoFile($id);
        Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
    }

    public static function updateCategory($id, $newName)
    {
        Core::getInstance()->db->update(self::$tableName, ['name' => $newName], ['id' => $id]);
    }

    public static function getCategories()
    {
        return Core::getInstance()->db->select(self::$tableName);
    }
}