<?php

use models\User;

if (\models\User::isUserAuthenticated()) {
    $user = User::getCurrentAuthenticatedUser();
} else {
    $user = null;
}

/** @var string $title */
/** @var string $siteName */
/** @var string $content */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $siteName ?> | <?= $title ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="/themes/light/css/mainStyle.css">
</head>
<body>
<div class="container">
    <header class="p-3 text-bg-light bg-white ">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-black text-decoration-none">
                    <img src="../../static/img/icon.png" alt="" class="w-75">
                </a>
                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                    <li><a href="/"
                           class="nav-link px-2  <?= $title === 'Main page' ? 'text-black' : 'text-secondary' ?>">Car
                            rental</a></li>
                    <li><a href="/cars"
                           class="nav-link px-2 <?= $title === 'Car park' ? 'text-black' : 'text-secondary' ?>">Car
                            park</a></li>
                    <li><a href="/brand/"
                           class="nav-link px-2 <?= $title === 'Brands' ? 'text-black' : 'text-secondary' ?>">Car
                            brands</a></li>
                    <li><a href="/category/"
                           class="nav-link px-2 <?= $title === 'Categories' ? 'text-black' : 'text-secondary' ?>">Categories</a>
                    </li>
                </ul>
                <div class="text-end">
                    <?php if (empty($user)): ?>
                        <a href="/user/login/" type="button" class="btn btn-outline-dark me-2">Login</a>
                        <a href="/user/register/" type="button" class="btn btn-success">Register</a>
                    <?php else: ?>
                        <ul class="dropdown-menu d-block position-static mx-0 shadow w-220px d-none position-absolute"
                            id="account-dropdown">
                            <li>
                                <a class="dropdown-item d-flex gap-2 align-items-center" href="/user/account/">
                                    <i class="bi bi-person-circle"></i>
                                    Account
                                </a>
                            </li>
                            <?php if (User::isAdmin() || User::isManager()): ?>
                                <li>
                                    <a class="dropdown-item d-flex gap-2 align-items-center" href="/rents/">
                                        <i class="bi bi-hourglass-split"></i>
                                        Rents
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (User::isAdmin()): ?>

                                <li>
                                    <a class="dropdown-item d-flex gap-2 align-items-center" href="/user/">
                                        <i class="bi bi-people-fill"></i>
                                        Users
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item d-flex gap-2 align-items-center" href="/statistics/">
                                        <i class="bi bi-graph-up"></i>
                                        Statistics
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a class="dropdown-item d-flex gap-2 align-items-center" href="/user/logout/">
                                    <i class="bi bi-box-arrow-left"></i>
                                    Log out
                                </a>
                            </li>
                        </ul>
                        <button class="btn btn-light"
                                id="account-button">
                            <i class="bi bi-person-circle me-2"></i><?= $user['firstname'] ?> <?= $user['lastname'] ?>
                        </button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>
</div>
<div class="container-fluid" style="padding: 0; min-height: 65vh">
    <main>
        <?= $content ?>
    </main>
</div>
<div class="container">
    <footer class="py-3 my-4 ">
        <ul class="nav justify-content-center border-bottom pb-3 mb-3">
            <li class="nav-item"><a href="/" class="nav-link px-2 text-muted">Main</a></li>
            <li class="nav-item"><a href="/cars/" class="nav-link px-2 text-muted">Car park</a></li>
            <li class="nav-item"><a href="/brand/" class="nav-link px-2 text-muted">Car brands</a></li>
            <li class="nav-item"><a href="/category/" class="nav-link px-2 text-muted">Categories</a></li>
        </ul>
        <p class="text-center text-muted">© 2022 Deyneka Yuriy</p>
    </footer>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
        crossorigin="anonymous"></script>
<script>
    const accountButton = document.body.querySelector('#account-button');
    const accountDropdown = document.body.querySelector('#account-dropdown');
    if (accountButton) {
        accountButton.addEventListener('click', (event) => {
            accountDropdown.classList.remove('d-none');
            accountDropdown.classList.add('d-block');
            event.stopPropagation();
        })
        document.body.addEventListener('click', (event) => {
            if (accountDropdown.classList.contains('d-block')) {
                accountDropdown.classList.remove('d-block');
                accountDropdown.classList.add('d-none');
            }
        });
    }
</script>
</body>
</html>