let carousel = document.querySelector('.carousel-inner');
let fullImgViewer = document.createElement('div');
let fullImg = document.createElement('img');
fullImg.style.maxWidth = '80%';
fullImg.style.maxHeight = '80%';
fullImgViewer.style.position = 'fixed';
fullImgViewer.style.width = '100%';
fullImgViewer.style.top = '0';
fullImgViewer.style.left = '0';
fullImgViewer.style.zIndex = '2';
fullImgViewer.style.height = '100vh';
fullImgViewer.style.display = 'none';
fullImgViewer.classList.add('full-img');
fullImgViewer.appendChild(fullImg);
document.body.appendChild(fullImgViewer);
carousel.addEventListener('click', (event) => {
    const target = event.target;
    if (target.tagName === 'IMG') {
        fullImg.src = target.src;
        fullImgViewer.style.display = 'flex';
    }
});
fullImgViewer.addEventListener('click', (event) => {
    fullImgViewer.style.display = 'none';
});
