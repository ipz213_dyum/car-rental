const searchForm = document.body.querySelector('#search-form');
const searchInput = document.body.querySelector('#search-input');
searchForm.addEventListener('submit', (event) => {
    event.preventDefault();
    window.location.replace("/cars/?search=" + searchInput.value.trim());
});
