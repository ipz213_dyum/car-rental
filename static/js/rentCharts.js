import {Chart} from "chart.js/auto";

const MONTHS = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];

const form = document.body.querySelector('#requestStatisticForm');
const formData = new FormData(form);
const month = formData.get('month');
const year = formData.get('year');
let rentsDatapoints = [];
let categoriesDatapoints = [];
let brandsDatapoints = [];
let brandsLabels = [];
let categoriesLabels = [];
let rentLabels = [];
let rentsRequestUrl = "/ajax/statistics/monthStatistic";
let categoriesRequestUrl = "/ajax/statistics/monthCategoryStatistic";
let brandsRequestUrl = "/ajax/statistics/monthBrandsStatistic";
let requestData = {year: year, month: month};
if (+month === 0) {
    rentsRequestUrl = "/ajax/statistics/yearStatistic";
    categoriesRequestUrl = "/ajax/statistics/yearCategoryStatistic";
    brandsRequestUrl = "/ajax/statistics/yearBrandsStatistic";

    rentLabels = MONTHS;
} else {
    for (let i = 1; i <= 31; i++) {
        rentLabels.push(i);
    }
}

$.ajax({
    type: "POST",
    url: rentsRequestUrl,
    data: requestData,
    success: function (res) {
        rentsDatapoints = JSON.parse(res);
        console.log(rentsDatapoints)
        rentsChart.data.datasets[0].data = rentsDatapoints;
        rentsChart.data.labels = rentLabels;
        rentsChart.update();
    }
});

$.ajax({
    type: "POST",
    url: categoriesRequestUrl,
    data: requestData,
    success: function (res) {
        let response = JSON.parse(res);
        console.log(response);
        for (const data of response) {
            categoriesDatapoints.push(+data['rentsCount']);
            categoriesLabels.push(data['category']);
        }
        categoriesChart.data.datasets[0].data = categoriesDatapoints;
        categoriesChart.data.labels = categoriesLabels;
        categoriesChart.update();
    }
});
$.ajax({
    type: "POST",
    url: brandsRequestUrl,
    data: requestData,
    success: function (res) {
        let response = JSON.parse(res);
        console.log(response);
        for (const data of response) {
            brandsDatapoints.push(+data['rentsCount']);
            brandsLabels.push(data['brand']);
        }
        brandsChart.data.datasets[0].data = brandsDatapoints;
        brandsChart.data.labels = brandsLabels;
        brandsChart.update();
    }
});
const rentsChartElem = document.getElementById("rentsChart").getContext('2d');
const categoriesChartElem = document.getElementById("categoriesChart").getContext('2d');
const brandsChartElem = document.getElementById("brandsChart").getContext('2d');
const rentsChart = new Chart(rentsChartElem, {
    type: 'line',
    data: {
        labels: rentLabels,
        datasets: [{
            label: "Rents",
            data: rentsDatapoints,
            fill: true,
            backgroundColor: [
                'rgba(75, 192, 192, .2)',
                // 'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
                'rgba(75, 192, 192, .7)',
                // 'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2,
            tension: 0.4
        }

        ]
    },
    options: {
        responsive: true,
        scales: {
            y: {
                display: true,
                title: {
                    display: true,
                    text: 'Cars in rent'
                },
                suggestedMin: 0,
                suggestedMax: 10
            },
            x: {
                display: true,
                title: {
                    display: true,
                    text: 'Days'
                },
            }
        },
        plugins: {
            legend: {
                display: false,
                position: 'bottom',
            },
        },
    }
});
const categoriesChart = new Chart(categoriesChartElem, {
    type: 'doughnut',
    data: {
        labels: categoriesLabels,
        datasets: [
            {
                label: 'Cars rented',
                data: categoriesDatapoints,
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true,
        plugins: {
            legend: {
                position: 'bottom',
            },
        },
    },

});

const brandsChart = new Chart(brandsChartElem, {
    type: 'doughnut',
    data: {
        labels: brandsLabels,
        datasets: [
            {
                label: 'Cars rented',
                data: brandsDatapoints,
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true,
        plugins: {
            legend: {
                position: 'bottom',
            },
        },
    },

});
