const selectTypeForm = document.body.querySelector('#select-type-form');
const usersTable = document.body.querySelector('.users-table');
const filterInput = document.body.querySelector('#filter-input');
if (filterInput.value !== '     ') {
    filterInput.focus();
    filterInput.selectionStart = filterInput.selectionEnd = filterInput.value.length
}
selectTypeForm.addEventListener('click', (event) => {
    if (event.target.classList.contains('btn-check')) {
        selectTypeForm.submit();
    }
});

usersTable.addEventListener('click', (event) => {
    let target = event.target;
    if (target.tagName === 'INPUT' && target.type === 'checkbox') {
        let access_level = 1;

        if (target.classList.contains('manager-checkbox') && target.checked) {
            access_level = 5;
        } else if (target.classList.contains('admin-checkbox') && target.checked) {
            access_level = 10;
        }
        let userId = target.dataset.userId;
        $.ajax({
            type: "POST",
            url: "/ajax/user/changeUserAccessLevel",
            data: {
                user_id: userId,
                access_level: access_level
            }
        });
    }
});