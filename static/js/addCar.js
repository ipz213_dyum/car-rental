let photosElem = document.body.querySelector('.car-photos');
let photosInput = document.body.querySelector('#photos');
photosInput.onchange = event => {
    const files = photosInput.files
    if (files) {
        photosElem.innerHTML = '';
        for (let i = 0; i < files.length; i++) {
            let carPhotoDiv = document.createElement('div');
            carPhotoDiv.classList.add('car-photo', 'col');
            let radio = document.createElement('input');
            radio.type = 'radio';
            radio.value = i.toString();
            radio.name = "main_photo";
            radio.id = `set-main-${i}`;
            let label = document.createElement('label');
            label.setAttribute('for', `set-main-${i}`);
            label.innerText = 'Main photo';
            label.classList.add('ms-1');
            let img = document.createElement('img');
            img.src = URL.createObjectURL(files[i]);
            img.classList.add('h-100', 'w-100');
            carPhotoDiv.append(img, radio, label);
            photosElem.appendChild(carPhotoDiv);
        }
    }
    // if (file) {
    //     blah.src = URL.createObjectURL(file)
    // }
}