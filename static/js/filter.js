const filterButton = document.body.querySelector('#filter-button');
const filterIcon = document.body.querySelector('#filter-button i');
const filterPanel = document.body.querySelector('#filter-panel');
const searchForm = document.body.querySelector('#search-form');
const searchInput = document.body.querySelector('#search-input');
const searchHidden = document.body.querySelector('#search-hidden');
searchInput.focus();
searchInput.selectionStart = searchInput.selectionEnd = searchInput.value.length

filterButton.addEventListener('click', (event) => {
    console.log('click');
    filterIcon.classList.toggle('bi-funnel-fill');
    filterIcon.classList.toggle('bi-funnel');
    filterPanel.classList.toggle('d-none');
});
searchForm.addEventListener('submit', (event) => {
    event.preventDefault();
    searchHidden.value = searchInput.value;
    filterPanel.submit();
});