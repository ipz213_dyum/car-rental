let carId = document.body.querySelector('#car-id');
let reviewForm = document.body.querySelector('#review-form');
let reviewError = document.body.querySelector('.review-error');
let rateStars = document.body.querySelector('.rate-stars');
let starsList = document.body.querySelectorAll('.star');
let reviewTextArea = document.body.querySelector('#review-form textarea');
let reviewsDiv = document.body.querySelector('.reviews-div');
let userReview;
let ratingsCount = document.body.querySelector('#ratings-count');
let ratingsCountRegex = new RegExp('\\d+');
starsList[0].classList.add('bi-star-fill');
starsList[0].classList.remove('bi-star');
let starsCount = 1;
rateStars.addEventListener('click', (event) => {
    if (event.target.classList.contains('star')) {
        let node = event.target;
        starsCount = node.dataset.index;
        starsList.forEach((item) => {
            item.classList.remove('bi-star-fill');
            item.classList.add('bi-star');
        });
        for (let i = starsCount; i >= 0; i--) {
            starsList[i].classList.remove('bi-star');
            starsList[i].classList.add('bi-star-fill');
        }
    }
});
reviewForm.addEventListener('submit', (event) => {
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: "/ajax/cars/sendReview",
        data: {rating: +starsCount + 1, text: reviewTextArea.value, car_id: carId.innerHTML},
        success: function (res) {
            let response = JSON.parse(res);
            if (response['error'] != null) {
                reviewError.innerHTML = response['error'];
            } else {
                let userReview = document.body.querySelector('.col.author-id-' + response['user_id']);
                if (userReview) {
                    userReview.remove();
                } else {
                    let count = +(ratingsCountRegex.exec(ratingsCount.innerHTML));
                    count++;
                    ratingsCount.innerHTML = `(${count}  ratings)`;
                }
                let col = document.createElement('div');
                let col10 = document.createElement('div');
                let col2 = document.createElement('div');
                col10.classList.add('col-10');
                col2.classList.add('col-2', 'd-flex', 'align-items-center', 'justify-content-end');
                col.classList.add('col', 'row', 'author-id-' + response['user_id'], 'review-' + response['review_id']);
                let deleteButton = document.createElement('button');
                deleteButton.classList.add('btn', 'btn-danger', 'delete-review', 'd-flex', 'gap-2');
                let deleteButtonIcon = document.createElement('i');
                deleteButtonIcon.classList.add('bi', 'bi-trash-fill', 'pe-none');
                deleteButton.dataset.reviewId = response['review_id'];
                let deleteButtonSpan = document.createElement('span');
                deleteButtonSpan.classList.add('d-none', 'd-md-block', 'pe-none');
                deleteButtonSpan.innerText = 'Delete';
                deleteButton.append(deleteButtonIcon, deleteButtonSpan);
                let ratingElem = document.createElement('div');
                ratingElem.classList.add('text-primary');
                let authorAndDateElem = document.createElement('div');
                authorAndDateElem.classList.add('mt-1', 'mb-2');
                let authorElem = document.createElement('span');
                authorElem.classList.add('review-author');
                authorElem.innerText = response['fullname'] + ' ';
                let dateElem = document.createElement('small');
                dateElem.classList.add('review-date', 'text-secondary');
                dateElem.innerText = response['date'];
                let textElem = document.createElement('p');
                textElem.classList.add('review-text');
                textElem.innerText = reviewTextArea.value;
                let hr = document.createElement('hr');
                for (let i = 0; i <= starsCount; i++) {
                    let star = document.createElement('i');
                    star.innerText = ' ';

                    star.classList.add('bi', 'bi-star-fill');
                    ratingElem.appendChild(star)
                }
                for (let i = 0; i < 4 - starsCount; i++) {
                    let star = document.createElement('i');
                    star.innerText = ' ';
                    star.classList.add('bi', 'bi-star');
                    ratingElem.appendChild(star);
                }
                authorAndDateElem.append(authorElem, dateElem);
                col10.append(ratingElem, authorAndDateElem, textElem);
                col2.appendChild(deleteButton);
                col.append(col10, col2, hr);
                reviewsDiv.prepend(col);
                reviewTextArea.innerHTML = '';
            }
        }
    });
});
reviewsDiv.addEventListener('click', (event) => {
    const target = event.target;
    if (target.tagName === 'BUTTON') {
        const reviewId = target.dataset.reviewId;
        $.ajax({
            type: "POST",
            url: "/ajax/cars/deleteReview",
            data: {id: reviewId},
            success: function (res) {


                if (res === '') {
                    (document.body.querySelector('.review-' + reviewId)).remove();
                    let count = +(ratingsCountRegex.exec(ratingsCount.innerHTML));
                    count--;
                    ratingsCount.innerHTML = `(${count}  ratings)`;
                }
            }
        });
    }
});

