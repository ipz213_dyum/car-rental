const carPhotosElem = document.querySelector('.car-photos');
const carPhotosList = document.querySelectorAll('.car-photo');
const setMainCheckBoxes = document.querySelectorAll('.car-photo > input');
const carId = +(document.querySelector('#car-id').innerHTML);
carPhotosElem.addEventListener('click', (event) => {
    let target = event.target;
    if (target.tagName === 'DIV' && target.classList.contains('car-photo')) {

        $.ajax({
            type: "POST",
            url: "/ajax/cars/deleteCarPhoto",
            data: {id: target.dataset.id},
            success: function (res) {
                carPhotosList.forEach((item) => {
                    if (item.dataset.id === target.dataset.id) {
                        item.remove();
                    }
                });
            }
        });

    } else if (target.tagName === 'INPUT') {
        target.checked = true;
        $.ajax({
            type: "POST",
            url: "/ajax/cars/setMainPhoto",
            data: {id: target.dataset.id, carId: carId},
            success: function (res) {
                console.log(res);
                setMainCheckBoxes.forEach((item) => {
                    if (item.dataset.id !== target.dataset.id) {
                        item.checked = false;
                    }
                });
            }
        });

    }
    event.stopPropagation();
});

