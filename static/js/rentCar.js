let sideBlock = document.body.querySelector('#side-block');
let rentForm = document.body.querySelector('#rent-form');
let startDateElem = document.body.querySelector("#start_date");
let endDateElem = document.body.querySelector("#end_date");
let locationInput = document.body.querySelector("#delivery_location");
let errorElem = document.body.querySelector("#rent-error");
let priceElem = document.body.querySelector("#price-text");
let daysElem = document.body.querySelector("#days-text");
let carPrice = priceElem.innerHTML.slice(1);

startDateElem.addEventListener('change', onDatesChanged);
endDateElem.addEventListener('change', onDatesChanged);

function onDatesChanged(event) {
    if (startDateElem.value && endDateElem.value) {
        let startDate = new Date(startDateElem.value);
        let endDate = new Date(endDateElem.value);
        if (startDate < endDate) {
            const diffTime = Math.abs(startDate - endDate);
            let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            if (diffDays === 0)
                diffDays = 1;
            let price = carPrice;
            price *= diffDays;
            daysElem.innerHTML = 'per ' + diffDays + ' days';
            priceElem.innerHTML = '$' + price;
        }
    }
}

if (rentForm) {
    rentForm.addEventListener('submit', (event) => {
        event.preventDefault();
        const startDate = startDateElem.value;
        const endDate = endDateElem.value;
        $.ajax({
            type: "POST",
            url: "/ajax/cars/rentCar",
            data: {
                car_id: carId.innerHTML,
                start_date: startDate,
                end_date: endDate,
                delivery_location: locationInput.value
            },
            success: function (res) {
                const response = JSON.parse(res);
                console.log(response);
                if (response['error'] != null) {
                    errorElem.innerHTML = response['error'];
                } else {
                    errorElem.innerHTML = '';
                    let statusDiv = document.createElement('div');
                    statusDiv.classList.add('text-primary');
                    let statusIcon = document.createElement('i');
                    statusIcon.classList.add('bi', 'bi-hourglass-split');
                    let statusSpan = document.createElement('span');
                    statusSpan.innerText = " Your rental request is in progress";
                    let cancelLink = document.createElement('a');
                    cancelLink.href = '/cars/cancelRent/' + response['rent_id'];
                    cancelLink.classList.add('btn', 'btn-danger', 'w-100', 'mt-3', 'd-block');
                    cancelLink.innerHTML = 'Cancel';
                    statusDiv.append(statusIcon, statusSpan, cancelLink);
                    sideBlock.append(statusDiv);
                    rentForm.remove();
                }
            }
        });
    });
}
