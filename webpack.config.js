const path = require('path');

module.exports = {
    entry: '/static/js/rentCharts.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'rentCharts.js',
    },
};