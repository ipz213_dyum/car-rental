<?php

namespace controllers;

use models\Brand;
use models\Category;
use models\Review;

class MainController extends \core\Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        $brands = Brand::getBrands();
        $categories = Category::getCategories();
        $reviews = Review::getGoodReviews();
        if (count($brands) > 6)
            $brands = array_slice($brands, 0, 6);
        if (count($categories) > 4)
            $categories = array_slice($categories, 0, 4);
        if (count($reviews) > 3)
            $reviews = array_slice($reviews, 0, 3);
        return $this->render(null, [
            'brands' => $brands,
            'categories' => $categories,
            'reviews' => $reviews
        ]);
    }

    public function errorAction($code)
    {
        switch ($code) {
            case 404:
                return $this->render('views/main/error-404.php');
                break;
            case 403:
                return $this->render('views/main/error-403.php');
                break;
        }
    }
}