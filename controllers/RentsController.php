<?php

namespace controllers;

use models\Rent;
use models\User;

class RentsController extends \core\Controller
{

    public function indexAction()
    {
        if (User::isAdmin() || User::isManager()) {
            $type = $_GET["type"];
            switch ($type) {
                case 'requests':
                    $rents = Rent::getRequests();
                    break;
                case 'active':
                    $rents = Rent::getActiveRents();
                    break;
                case 'ended':
                    $rents = Rent::getEndedRents();
                    break;
                default:
                    $rents = Rent::getRents();
            }
            if ($type !== 'requests') {
                $requestsCount = count(Rent::getRequests());
            } else {
                $requestsCount = count($rents);
            }
            return $this->render(null, [
                'rents' => $rents,
                'requestsCount' => $requestsCount,
            ]);
        } else {
            return $this->error(403);
        }
    }

    public function acceptAction($params)
    {
        $type = $_GET["type"];
        if (User::isAdmin() || User::isManager()) {
            $id = $params[0];
            if ($id > 0) {
                Rent::approveRentRequest($id);
                $this->redirect('/rents/?type=' . $type);
            } else {
                return $this->error(404);
            }
        } else {
            return $this->error(403);
        }
    }

    public function declineAction($params)
    {
        $type = $_GET["type"];
        if (User::isAdmin() || User::isManager()) {
            $id = $params[0];
            if ($id > 0) {
                Rent::declineRentRequest($id);
                $this->redirect('/rents/?type=' . $type);
            } else {
                return $this->error(404);
            }
        } else {
            return $this->error(403);
        }
    }
}