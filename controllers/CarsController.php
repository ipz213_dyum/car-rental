<?php

namespace controllers;

use core\Controller;
use core\Core;
use core\Utils;
use models\Brand;
use models\Car;
use models\Category;
use models\FuelType;
use models\Rent;
use models\Review;
use models\TransmissionType;
use models\User;
use mysql_xdevapi\Result;

class CarsController extends Controller
{
    public function indexAction()
    {
        $cars = Car::getCars(null, null);
        $brands = Brand::getBrands();
        $categories = Category::getCategories();
        $transmissions = TransmissionType::getTransmissionTypes();
        $fuelTypes = FuelType::getFuelTypes();
        $cities = Car::getCities();
        $filterFields = ['sortBy', 'sortType', 'brands', 'categories', 'transmissions', 'fuel_types', 'search', 'cities'];
        $isFiltered = false;
        foreach (array_keys($_GET) as $getParam) {
            if (in_array($getParam, $filterFields)) {
                $isFiltered = true;
                break;
            }
        }
        $sortField = $_GET["sortBy"] ?? 'avg_rating';
        $sortType = $_GET["sortType"] ?? 'desc';
        if (!User::isAdmin()) {
            $cars = Utils::filterByFieldInArray($cars, 'visible', [1]);
        }
        if ($isFiltered) {
            if (!empty($_GET["minPrice"]) || !empty($_GET["maxPrice"]))
                $cars = Utils::filterArrayByMaxAndMin($cars, 'price', $_GET["minPrice"] ?? null,
                    $_GET["maxPrice"] ?? null);
            if (!empty($_GET["brands"]))
                $cars = Utils::filterByFieldInArray($cars, 'brand_id', $_GET["brands"]);
            if (!empty($_GET["categories"]))
                $cars = Utils::filterByFieldInArray($cars, 'category_id', $_GET["categories"]);
            if (!empty($_GET["transmissions"]))
                $cars = Utils::filterByFieldInArray($cars, 'transmission_id', $_GET["transmissions"]);
            if (!empty($_GET["fuel_types"]))
                $cars = Utils::filterByFieldInArray($cars, 'fuel_type_id', $_GET["fuel_types"]);
            if (!empty($_GET["search"]))
                $cars = Utils::filterByFieldContainsArrayElement($cars, 'model', explode(' ', strtolower(trim($_GET["search"]))));
            if (!empty($_GET["cities"]))
                $cars = Utils::filterByFieldInArray($cars, 'city', $_GET["cities"]);

        }
        if (count($cars) > 0) {
            if ($sortType === 'asc')
                $cars = Utils::sortAsc($cars, $sortField);
            else
                $cars = Utils::sortDesc($cars, $sortField);
        }
        return $this->render(null, [
            'cars' => $cars,
            'brands' => $brands,
            'categories' => $categories,
            'transmissions' => $transmissions,
            'fuelTypes' => $fuelTypes,
            'isFiltered' => $isFiltered,
            'cities' => $cities,
        ]);
    }

    public function viewAction($params)
    {
        $id = intval($params[0]);
        if (Car::isCarById($id)) {
            $car = Car::getCarById($id);
            if ($car['visible'] == 0 && !User::isAdmin()) {
                return $this->error(404);
            }
            $car_photos = Car::getCarPhotosById($id);
            $car_reviews = Review::getReviewsByCarId($id);
            $car_trips_count = Car::getTripsCountById($id);
            $car_brand = Brand::getBrandById($car['brand_id']);
            $car_category = Category::getCategoryById($car['category_id']);
            $car_fuel_type = FuelType::getFuelTypeById($car['fuel_type_id']);
            $car_transmission = TransmissionType::getTransmissionById($car['transmission_id']);
            if (User::isUserAuthenticated())
                $user_rent = Rent::getUserCurrentCarRent(User::getCurrentAuthenticatedUser()['id'], $car['id']);
//            if (Core::getInstance()->requestMethod === 'POST') {
//                $current_date = date('Y-m-d', time());
//                $errors = [];
//                $model = $_POST;
//                if ($_POST['type'] === 'rent') {
//                    if ($_POST['start_date'] < $current_date) {
//                        $errors['start_date'] = 'Wrong date';
//                    }
//                    if ($_POST['end_date'] < $current_date) {
//                        $errors['end_date'] = 'Wrong date';
//                    }
//                    if (empty($_POST['delivery_location'])) {
//                        $errors['delivery_location'] = 'Empty location';
//                    }
//                    if (empty($errors)) {
//                        $rentId = Rent::createRentRequest($id, $_POST['start_date'], $_POST['end_date'], $_POST['delivery_location']);
//                        return $this->render(null,
//                            [
//                                'car' => $car,
//                                'car_photos' => $car_photos,
//                                'car_reviews' => $car_reviews,
//                                'car_brand' => $car_brand,
//                                'car_category' => $car_category,
//                                'car_fuel_type' => $car_fuel_type,
//                                'car_transmission' => $car_transmission,
//                                'user_rent' => Rent::getRentById($rentId),
//                                'car_trips_count' => $car_trips_count
//                            ]);
//                    } else {
//                        return $this->render(null,
//                            [
//                                'car' => $car,
//                                'car_photos' => $car_photos,
//                                'car_reviews' => $car_reviews,
//                                'car_brand' => $car_brand,
//                                'car_category' => $car_category,
//                                'car_fuel_type' => $car_fuel_type,
//                                'car_transmission' => $car_transmission,
//                                'errors' => $errors,
//                                'model' => $model,
//                                'user_rent' => $user_rent,
//                                'car_trips_count' => $car_trips_count
//
//                            ]);
//                    }
//                }
//            } else {
            return $this->render(null,
                [
                    'car' => $car,
                    'car_photos' => $car_photos,
                    'car_reviews' => $car_reviews,
                    'car_brand' => $car_brand,
                    'car_category' => $car_category,
                    'car_fuel_type' => $car_fuel_type,
                    'car_transmission' => $car_transmission,
                    'user_rent' => $user_rent,
                    'car_trips_count' => $car_trips_count

                ]);

        } else
            return $this->error(404);
    }

    public function cancelRentAction($params)
    {
        $redirect = $_GET["redirect"];
        $id = $params[0];
        if (Rent::isRentById($id)) {
            $rent = Rent::getRentById($params[0]);
            if (User::isUserAuthenticated() && User::getCurrentAuthenticatedUser()['id'] == $rent['user_id']) {
                Rent::cancelRentById($id);
                if (empty($redirect))
                    $this->redirect('/cars/view/' . $rent['car_id']);
                else
                    $this->redirect($redirect);
            } else {
                return $this->error(403);
            }
        } else {
            return $this->error(404);
        }

    }

    public function addAction()
    {
        if (User::isAdmin()) {
            $brands = Brand::getBrands();
            $categories = Category::getCategories();
            $transmissions = TransmissionType::getTransmissionTypes();
            $fuelTypes = FuelType::getFuelTypes();
            if (Core::getInstance()->requestMethod === 'POST') {
                $model = $_POST;
                $errors = [];
                if ($_POST['brand_id'] <= 0) {
                    $errors['brand_id'] = 'Wrong brand';
                }
                $_POST['model'] = trim($_POST['model']);
                if (empty($_POST['model'])) {
                    $errors['model'] = 'Empty model value';
                }
                if ($_POST['price'] <= 0) {
                    $errors['price'] = 'Wrong price value';
                }
                if ($_POST['kilometrage'] < 0) {
                    $errors['kilometrage'] = 'Wrong kilometrage value';
                }
                if ($_POST['transmission_id'] <= 0) {
                    $errors['transmission_id'] = 'Wrong transmission value';
                }
                if ($_POST['fuel_type_id'] <= 0) {
                    $errors['fuel_type_id'] = 'Wrong fuel type value';
                }
                if ($_POST['fuel_consumption'] < 0) {
                    $errors['fuel_consumption'] = 'Wrong consumption value';
                }
                if (empty($_POST['city'])) {
                    $errors['city'] = 'Wrong city';
                }
                if ($_POST['seats'] <= 0) {
                    $errors['seats'] = 'Wrong seats count';
                }
                $_POST['description'] = trim($_POST['description']);
                if ($_POST['description'] == '') {
                    $errors['description'] = 'Fill description';
                }
                foreach ($_FILES['photos']['name'] as $filename) {
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (!in_array($ext, ['jpg', 'jpeg'])) {
                        $errors['photos'] = 'photos must be .jpg or .jpeg';
                    }
                }
                $mainPhotoIndex = $_POST['main_photo'];
                $mainPhotoFile = $_FILES['photos']['tmp_name'][$mainPhotoIndex];
                if (!empty($errors)) {
                    return $this->render(null, [
                        'brands' => $brands,
                        'categories' => $categories,
                        'transmissions' => $transmissions,
                        'fuelTypes' => $fuelTypes,
                        'model' => $model,
                        'errors' => $errors,
                    ]);
                } else {
                    $carId = Car::addCar($_POST['model'], $_POST['price'], $_POST['kilometrage'], $_POST['transmission_id'],
                        $_POST['fuel_type_id'], $_POST['fuel_consumption'], $_POST['seats'], $_POST['city'], $_POST['description'],
                        empty($_POST['visible']) ? 0 : 1, $_POST['brand_id'], $_POST['category_id']);

                    $mainPhotoId = Car::addPhotos($carId, $_FILES['photos']['tmp_name'], $mainPhotoIndex);
                    Car::updateCar($carId, [
                        'main_photo_id' => $mainPhotoId
                    ]);
                    $this->redirect("/cars/view/{$carId}");
                }
            } else
                return $this->render(null, ['brands' => $brands,
                    'categories' => $categories,
                    'transmissions' => $transmissions,
                    'fuelTypes' => $fuelTypes,]);
        } else {
            $this->error(403);
        }
    }

    public function editAction($params)
    {

        if (User::isAdmin()) {
            $id = $params[0];
            if (Car::isCarById($id)) {
                $car = Car::getCarById($id);
                $car['photos'] = Car::getCarPhotosById($id);
                $brands = Brand::getBrands();
                $categories = Category::getCategories();
                $transmissions = TransmissionType::getTransmissionTypes();
                $fuelTypes = FuelType::getFuelTypes();

                if (Core::getInstance()->requestMethod === 'POST') {

                    $model = $_POST;
                    $model['photos'] = $car['photos'];
                    $errors = [];
                    if ($_POST['brand_id'] <= 0) {
                        $errors['brand_id'] = 'Wrong brand';
                    }
                    $_POST['model'] = trim($_POST['model']);
                    if (empty($_POST['model'])) {
                        $errors['model'] = 'Empty model value';
                    }
                    if ($_POST['price'] <= 0) {
                        $errors['price'] = 'Wrong price value';
                    }
                    if ($_POST['kilometrage'] < 0) {
                        $errors['kilometrage'] = 'Wrong kilometrage value';
                    }
                    if ($_POST['transmission_id'] <= 0) {
                        $errors['transmission_id'] = 'Wrong transmission value';
                    }
                    if ($_POST['fuel_type_id'] <= 0) {
                        $errors['fuel_type_id'] = 'Wrong fuel type value';
                    }
                    if ($_POST['fuel_consumption'] < 0) {
                        $errors['fuel_consumption'] = 'Wrong consumption value';
                    }
                    if (empty($_POST['city'])) {
                        $errors['city'] = 'Wrong city';
                    }
                    if ($_POST['seats'] <= 0) {
                        $errors['seats'] = 'Wrong seats count';
                    }
                    $_POST['description'] = trim($_POST['description']);
                    if ($_POST['description'] == '') {
                        $errors['description'] = 'Fill description';
                    }
                    if ($_FILES['photos']['error'][0] != 4) {
                        foreach ($_FILES['photos']['name'] as $filename) {
                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            if (!in_array($ext, ['jpg', 'jpeg'])) {
                                $errors['photos'] = 'photos must be .jpg or .jpeg';
                            }
                        }
                    }

                    if (!empty($errors)) {
                        return $this->render(null, [
                            'brands' => $brands,
                            'categories' => $categories,
                            'transmissions' => $transmissions,
                            'fuelTypes' => $fuelTypes,
                            'model' => $model,
                            'errors' => $errors,
                            'carId' => $car['id'],
                            'mainPhotoId' => $car['main_photo_id'],
                        ]);

                    } else {
                        Car::updateCar($id,
                            [
                                'model' => $_POST['model'],
                                'price' => $_POST['price'],
                                'kilometrage' => $_POST['kilometrage'],
                                'transmission_id' => $_POST['transmission_id'],
                                'fuel_type_id' => $_POST['fuel_type_id'],
                                'fuel_consumption' => $_POST['fuel_consumption'],
                                'seats' => $_POST['seats'],
                                'city' => $_POST['city'],
                                'description' => $_POST['description'],
                                'visible' => empty($_POST['visible']) ? 0 : 1,
                                'brand_id' => $_POST['brand_id'],
                                'category_id' => $_POST['category_id']
                            ]);
                        if ($_FILES['photos']['error'][0] != 4) {
                            Car::addPhotos($id, $_FILES['photos']['tmp_name']);
                        }

                        $this->redirect('/cars/');
                    }
                } else
                    return $this->render(null, ['brands' => $brands,
                        'categories' => $categories,
                        'transmissions' => $transmissions,
                        'fuelTypes' => $fuelTypes,
                        'model' => $car,
                        'carId' => $car['id'],
                        'mainPhotoId' => $car['main_photo_id'],
                    ]);
            } else {
                $this->error(404);
            }
        } else {
            $this->error(403);
        }
    }

    public function deleteAction($params)
    {
        if (User::isAdmin()) {
            $id = $params[0];
            $yes = boolval($params[1] === 'yes');
            if (Car::isCarById($id)) {
                $car = Car::getCarById($id);
                if ($yes) {
                    Car::deleteCarById($car['id']);
                    $this->redirect('/cars/');
                }
                return $this->render(null, ['car' => $car]);
            } else {
                $this->error(404);
            }
        } else {
            $this->error(403);
        }
    }

    public function deleteCarPhotoAjaxAction()
    {
        if (User::isAdmin()) {
            $photoId = $_POST['id'];
            Car::deletePhotoById($photoId);
        }
    }

    public function setMainPhotoAjaxAction()
    {
        if (User::isAdmin()) {
            Car::setMainPhotoForCar($_POST["carId"], $_POST["id"]);
        }
    }

    public function rentCarAjaxAction()
    {
        if (User::isUserAuthenticated()) {
            $startDate = $_POST["start_date"];
            $endDate = $_POST["end_date"];
            $currentDate = date('Y-m-d');
            $carId = $_POST["car_id"];
            $deliveryLocation = trim($_POST["delivery_location"]);
            $result = [];
            if ($endDate <= $currentDate) {
                $result['error'] = "Wrong end date";
            } else if ($startDate >= $endDate || $startDate <= $currentDate) {
                $result['error'] = "Wrong start date";
            } else if ($deliveryLocation === '') {
                $result['error'] = "Enter delivery location";
            }
            if (empty($result['error'])) {
                if (!Rent::isCarRentedForDate($carId, $startDate, $endDate)) {
                    $result['rent_id'] = Rent::createRentRequest($carId, $startDate, $endDate, $deliveryLocation);
                } else {
                    $result['error'] = "The car is already rented for these dates";
                }
            }
            echo json_encode($result);
        }
    }

    public function sendReviewAjaxAction()
    {
        if (User::isUserAuthenticated()) {
            $result = [];
            $car_id = intval($_POST['car_id']);
            $rating = intval($_POST['rating']);
            $text = trim($_POST['text']);
            if (strlen($text) > 250) {
                $result['error'] = 'Text too long';
            }
            if ($rating > 5 || $rating < 1) {
                $result['error'] = 'Wrong rating';
            }
            if (empty($result['error'])) {
                $currentUser = User::getCurrentAuthenticatedUser();
                $review_id = Review::addReview($car_id, $currentUser['id'], $text, $rating);
                $result['fullname'] = $currentUser['firstname'] . ' ' . $currentUser['lastname'];
                $result['date'] = date('d M Y');
                $result['user_id'] = $currentUser['id'];
                $result['review_id'] = $review_id;
            }
            echo json_encode($result);
        }
    }

    public function deleteReviewAjaxAction()
    {
        $result = [];
        $id = $_POST["id"];
        if (Review::isReviewById($id)) {
            $review = Review::getReviewById($id);
            if (User::isAdmin() || User::getCurrentAuthenticatedUser()['id'] == $review['user_id']) {
                Review::deleteReviewById($id);
            } else {
                $result['error'] = 'Have no rights';
            }
        } else {
            $result['error'] = 'Review does not exists';
        }
        return json_encode($result);
    }
}