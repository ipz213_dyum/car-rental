<?php

namespace controllers;

use core\Core;
use models\Product;
use models\Rent;
use models\Review;
use models\User;

class UserController extends \core\Controller
{
    public function registerAction()
    {
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            $phonePattern = "/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/";
            $_POST['login'] = trim($_POST['login']);
            $_POST['phone_number'] = trim($_POST['phone_number']);
            $_POST['firstname'] = trim($_POST['firstname']);
            $_POST['lastname'] = trim($_POST['lastname']);
            if (strlen($_POST["firstname"]) < 1) {
                $errors['firstname'] = 'First name too short';
            }
            if (strlen($_POST["lastname"]) < 1) {
                $errors['lastname'] = 'Last name too short';
            }
            if (!filter_var($_POST['login'], FILTER_VALIDATE_EMAIL))
                $errors['login'] = 'Wrong email';
            if (User::isLoginExists($_POST['login']))
                $errors['login'] = 'Email has already been taken';
            if (preg_match($phonePattern, $_POST["phone_number"]) != 1) {
                $errors['phone_number'] = 'Wrong phone number';
            }
            if (User::isPhoneNumberExists($_POST['phone_number'])) {
                $errors['phone_number'] = 'An account with this phone number already exists';
            }
            if (strlen($_POST['password']) < 6) {
                $errors['password'] = 'Password too short';
            }
            if ($_POST['password'] !== $_POST['password-repeat'])
                $errors['password-repeat'] = 'Passwords do not match';
            if (count($errors) > 0) {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            } else {
                User::addUser($_POST['login'], $_POST['password'], $_POST['firstname'], $_POST['lastname'], $_POST["phone_number"]);
                $user = User::getUserByLoginAndPassword($_POST['login'], $_POST['password']);
                User::authenticateUser($user);
                $this->redirect('/');
            }

        } else
            return $this->render();
    }

    public function loginAction()
    {
        if (User::isUserAuthenticated())
            $this->redirect('/');
        $error = null;
        $model = [];
        if (Core::getInstance()->requestMethod === 'POST') {
            $user = User::getUserByLoginAndPassword($_POST['login'], $_POST['password']);
            $model['login'] = $_POST["login"];
            if (empty($user)) {
                $error = 'Incorrect login or password';
            } else {
                User::authenticateUser($user);
                $this->redirect('/');
            }
        }
        return $this->render(null, [
            'error' => $error,
            'model' => $model]);
    }

    public function logoutAction()
    {
        User::logoutUser();
        $this->redirect('/user/login/');
    }

    public function indexAction()
    {
        if (User::isAdmin()) {
            $type = $_GET["type"];
            $filter = strtolower(trim($_GET["filter"]));
            $filterArray = explode(' ', $filter);
            if ($type === 'admins') {
                $users = User::getAdmins();
            } else if ($type === 'managers') {
                $users = User::getManagers();
            } else {
                $users = User::getUsers();
            }
            if (!empty($filter)) {
                $fields = array_keys($users[0]);
                foreach ($users as $k => $user) {
                    $contains = false;
                    foreach ($fields as $field) {
                        if (in_array(strtolower($user[$field]), $filterArray)) {
                            $contains = true;
                            break;
                        }
                    }
                    if (!$contains) {
                        unset($users[$k]);
                    }
                }

                return $this->render(null, [
                    'users' => $users,
                    'filter' => $filter,
                ]);
            }
            return $this->render(null, [
                'users' => $users
            ]);
        } else {
            return $this->error(403);
        }
    }

    public function changeUserAccessLevelAjaxAction()
    {
        if (User::isAdmin()) {
            $userId = $_POST["user_id"];
            $access_level = $_POST["access_level"];
            if ($access_level == 5) {
                User::giveManagerLevel($userId);
            } else if ($access_level == 10) {
                User::giveAdminLevel($userId);
            } else {
                User::removeAdminLevel($userId);
            }
        }
    }

    public function accountAction()
    {
        if (User::isUserAuthenticated()) {
            $user = User::getCurrentAuthenticatedUser();
            $rents = Rent::getUserRents($user['id']);
            if (Core::getInstance()->requestMethod === "POST") {
                $model = $_POST;
                $errors = [];
                $phonePattern = "/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/";
                $_POST['login'] = trim($_POST['login']);
                $_POST['phone_number'] = trim($_POST['phone_number']);
                $_POST['firstname'] = trim($_POST['firstname']);
                $_POST['lastname'] = trim($_POST['lastname']);
                $_POST['password-new'] = trim($_POST['password-new']);
                if (strlen($_POST["firstname"]) < 1) {
                    $errors['firstname'] = 'First name too short';
                }
                if (strlen($_POST["lastname"]) < 1) {
                    $errors['lastname'] = 'Last name too short';
                }
                if (!filter_var($_POST['login'], FILTER_VALIDATE_EMAIL))
                    $errors['login'] = 'Wrong email';
                if ($_POST["login"] !== $user['login'] && User::isLoginExists($_POST['login']))
                    $errors['login'] = 'Email has already been taken';
                if (preg_match($phonePattern, $_POST["phone_number"]) != 1) {
                    $errors['phone_number'] = 'Wrong phone number';
                }
                if ($_POST["phone_number"] !== $user['phone_number'] && User::isPhoneNumberExists($_POST['phone_number'])) {
                    $errors['phone_number'] = 'An account with this phone number already exists';
                }
                if (!empty($_POST["password-old"]) && !empty($_POST["password-new"])) {
                    if (User::getUserByLoginAndPassword(User::getCurrentAuthenticatedUser()['login'], $_POST["password-old"]) === null) {
                        $errors['password-old'] = "Wrong old password";
                    }
                    if (strlen($_POST['password-new']) < 6) {
                        $errors['password-new'] = 'Password too short';
                    }
                }
                if (empty($errors)) {

                    User::updateUser($user['id'], [
                        'login' => $_POST["login"],
                        'phone_number' => $_POST["phone_number"],
                        'firstname' => $_POST["firstname"],
                        'lastname' => $_POST["lastname"],
                    ]);
                    if (!empty($_POST["password-old"]) && !empty($_POST["password-new"])) {
                        User::updatePassword($_POST["password-new"]);
                    }
                    $this->redirect('/user/account/');
                } else {
                    return $this->render(null, [
                        'user' => $user,
                        'model' => $model,
                        'errors' => $errors,
                        'rents' => $rents
                    ]);
                }
            }
            return $this->render(null, [
                'user' => $user,
                'rents' => $rents
            ]);
        } else {
            $this->redirect('/user/login/');
        }
    }
}