<?php

namespace controllers;

use models\Rent;
use models\Statistics;
use models\User;

class StatisticsController extends \core\Controller
{
    public function indexAction()
    {
        if (User::isAdmin()) {
            $month = $_GET["month"];
            $year = $_GET["year"];
            $label = "";
            $model = [];
            if ($month != null && $year != null) {
                if ($month == 0) {
                    $startDate = "$year-01-01";
                    $endDate = "$year-12-31";
                    $label = "All months, $year";
                } else {
                    $startDate = "$year-$month-01";
                    $maxDays = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
                    $endDate = "$year-$month-$maxDays";
                    $monthName = \DateTime::createFromFormat('!m', $month);
                    $monthName = $monthName->format('F');
                    $label = "$monthName, $year";
                }
            } else {
                $startDate = date("Y-m-01");
                $maxDays = date('t');
                $endDate = date("Y-m-$maxDays");
                $label = "This month";
                $model['month'] = date('m');
                $model['year'] = date('Y');
            }
            $statistics = Statistics::getStatistics($startDate, $endDate);
            $rents = Rent::getRentsInRange($startDate, $endDate);

            $model['label'] = $label;
            $model = array_merge($model, $_GET);
            return $this->render(null, [
                'moneyEarned' => $statistics['moneyEarned'],
                'rentsCount' => $statistics['rentsCount'],
                'avgRentPeriod' => $statistics['avgRentPeriod'],
                'rents' => $rents,
                'model' => $model
            ]);
        } else {
            return $this->error(403);
        }
    }

    function monthStatisticAjaxAction()
    {
        if (User::isAdmin()) {
            $year = $_POST['year'];
            $month = $_POST['month'];
            $days = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
            $data = Statistics::getRentStatisticForMonth($year, $month);
            $result = null;
            for ($i = 1; $i <= $days; $i++) {
                $result[$i] = 0;
                foreach ($data as $d) {
                    if (intval($d['day']) == $i) {
                        $result[$i] = intval($d['rentsCount']);
                        break;
                    }
                }
            }

            echo json_encode(array_values($result));
        }
    }

    function yearStatisticAjaxAction()
    {
        if (User::isAdmin()) {
            $year = $_POST['year'];
            $data = Statistics::getRentStatisticForYear($year);
            $result = null;
            for ($i = 1; $i <= 12; $i++) {
                $result[$i] = 0;
                foreach ($data as $d) {
                    if (intval($d['month']) == $i) {
                        $result[$i] = intval($d['rentsCount']);
                        break;
                    }
                }
            }
            echo json_encode(array_values($result));
        }
    }

    function monthCategoryStatisticAjaxAction()
    {
        if (User::isAdmin()) {
            $year = $_POST['year'];
            $month = $_POST['month'];
            $days = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
            $data = Statistics::getRentCategoryStatisticForMonth($year, $month);

            echo json_encode($data);
        }
    }

    function yearCategoryStatisticAjaxAction()
    {
        if (User::isAdmin()) {
            $year = $_POST['year'];
            $data = Statistics::getRentCategoryStatisticForYear($year);
            echo json_encode($data);
        }
    }

    function monthBrandsStatisticAjaxAction()
    {
        if (User::isAdmin()) {
            $year = $_POST['year'];
            $month = $_POST['month'];
            $days = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
            $data = Statistics::getRentBrandsStatisticForMonth($year, $month);

            echo json_encode($data);
        }
    }

    function yearBrandsStatisticAjaxAction()
    {
        if (User::isAdmin()) {
            $year = $_POST['year'];
            $data = Statistics::getRentBrandsStatisticForYear($year);
            echo json_encode($data);
        }
    }
}