<?php

namespace controllers;

use core\Core;
use models\Brand;
use models\User;

class BrandController extends \core\Controller
{
    public function indexAction()
    {
        $brands = Brand::getBrands();
        return $this->render(null, ['brands' => $brands]);
    }

    public function addAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            $_POST['name'] = trim($_POST['name']);
            $ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
            if (empty($_POST['name']))
                $errors['name'] = 'Empty brand name';
            if ($_FILES['photo']['size'] == 0)
                $errors['photo'] = 'Empty brand photo';
            if ($ext !== 'png')
                $errors['photo'] = 'Brand photo must be .png';

            if (empty($errors)) {
                Brand::addBrand($_POST['name'], $_FILES['photo']['tmp_name']);
                $this->redirect('/brand/');
            } else {
                $model = $_POST;
                return $this->render(null, ['errors' => $errors, 'model' => $model]);
            }
        }
        return $this->render();
    }

    public function editAction($params)
    {
        if (!User::isAdmin())
            return $this->error(403);
        $id = intval($params[0]);
        if (Brand::isBrandById($id)) {
            $brand = Brand::getBrandById($id);
            if (Core::getInstance()->requestMethod == 'POST') {
                $errors = [];
                $_POST['name'] = trim($_POST['name']);
                $ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
                if (empty($_POST['name']))
                    $errors['name'] = 'Empty brand name';

                if ($_FILES['photo']['size'] != 0 && $ext !== 'png')
                    $errors['photo'] = 'Brand photo must be .png';

                if (empty($errors)) {
                    Brand::updateBrand($id, $_POST['name']);
                    if ($_FILES['photo']['size'] > 0)
                        Brand::changePhoto($id, $_FILES['photo']['tmp_name']);
                    $this->redirect('/brand/');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'brand' => $brand
                    ]);
                }
            }
            return $this->render(null, [
                'brand' => $brand
            ]);
        } else {
            return $this->error(404);
        }

    }

    public function deleteAction($params)
    {
        $id = $params[0];
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            return $this->error(403);
        if (Brand::isBrandById($id)) {
            $brand = Brand::getBrandById($id);
            if ($yes) {
                $filePath = 'files/brand/' . $brand['photo'];
                if (is_file($filePath))
                    unlink($filePath);
                Brand::deleteBrandById($brand['id']);
                $this->redirect('/brand/');
            }
            return $this->render(null, ['brand' => $brand]);
        } else {
            return $this->error(404);
        }
    }
}