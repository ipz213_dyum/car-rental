<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Brand;
use models\Category;
use models\User;

class CategoryController extends Controller
{
    public function indexAction()
    {
        $categories = Category::getCategories();
        return $this->render(null, ['categories' => $categories]);

    }

    public function addAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            $_POST['name'] = trim($_POST['name']);
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if (empty($_POST['name']))
                $errors['name'] = 'Empty category name';
            if (empty($_FILES['file']))
                $errors['file'] = 'Empty category file';
            if (!in_array($ext, ['jpg', 'jpeg']))
                $errors['file'] = 'Category photo must be .jpg or .jpeg';
            if (empty($errors)) {
                Category::addCategory($_POST['name'], $_FILES['file']['tmp_name']);
                $this->redirect('/category/');
            } else {
                $model = $_POST;
                return $this->render(null, ['errors' => $errors, 'model' => $model]);
            }
        }
        return $this->render();
    }

    public function editAction($params)
    {
        if (!User::isAdmin())
            return $this->error(403);
        $id = intval($params[0]);
        if (Category::isCategoryById($id)) {
            $category = Category::getCategoryById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $errors = [];
                $_POST['name'] = trim($_POST['name']);
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if (empty($_POST['name']))
                    $errors['name'] = 'Empty category name';
                if (empty($_FILES['file']))
                    $errors['file'] = 'Empty category file';
                if ($_FILES['file']['size'] != 0 && !in_array($ext, ['jpg', 'jpeg']))
                    $errors['file'] = 'Category photo must be .jpg or .jpeg';
                var_dump($_FILES);
                if (empty($errors)) {
                    Category::updateCategory($id, $_POST['name']);
                    if (!empty($_FILES['file']['tmp_name']))
                        Category::changePhoto($id, $_FILES['file']['tmp_name']);
                    $this->redirect('/category/');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'category' => $category
                    ]);
                }


            }
            return $this->render(null, [
                'category' => $category
            ]);
        } else {
            return $this->error(404);
        }
    }

    public function deleteAction($params)
    {
        $id = $params[0];
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            return $this->error(403);
        if (Category::isCategoryById($id)) {
            $category = Category::getCategoryById($id);
            if ($yes) {
                $filePath = 'files/category/' . $category['photo'];
                if (is_file($filePath))
                    unlink($filePath);
                Category::deleteCategoryById($category['id']);
                $this->redirect('/category/');
            }
            return $this->render(null, ['category' => $category]);
        } else {
            return $this->error(404);
        }
    }
}