<?php
/** @var array $cars */
/** @var array $categories */
/** @var array $brands */
/** @var array $transmissions */
/** @var array $fuelTypes */
/** @var array $cities */

/** @var bool $isFiltered */


use models\User;

\core\Core::getInstance()->pageParams['title'] = 'Car park';

?>
<style>
    i::before {
    }

    .car-hidden {
        position: relative;
    }

    .car-hidden img {
        opacity: .2;
    }

    body {
        overflow-x: hidden;
    }
    h5{
        font-size: 1.1rem;
    }
    .car-hidden::before {
        z-index: 1;
        position: absolute;
        content: 'Hidden';
        font-weight: bold;
        font-size: 30px;
        width: 100%;
        height: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        top: 0;
        left: 0;
    }
    div.rating, div.city{
        font-size: 0.9rem;
    }
</style>
<link rel="stylesheet" href="../../themes/light/css/mainStyle.css">
<div class="container ">
    <div class="mt-5 mb-2 mb-md-4 d-flex flex-column flex-md-row  align-items-md-center gap-3">
        <h2 class="d-inline ">Our car park</h2>
        <form action="" method="get" class="d-flex gap-3 ms-2" id="search-form">
            <input type="text" class="form-control" placeholder="Search" id="search-input"
                   value="<?= $_GET["search"] ?>">
            <button class="btn btn-primary">
                <i class="bi bi-search"></i>
            </button>
        </form>
        <button class="btn btn-secondary" id="filter-button">
            <?php if ($isFiltered): ?>
                <i class="bi bi-funnel-fill"></i>
            <?php else: ?>
                <i class="bi bi-funnel"></i>
            <?php endif; ?>
            <span class="ms-2">Filter</span>
        </button>
        <?php if (\models\User::isAdmin()): ?>
            <a href="/cars/add/" class="btn btn-success">
                <i class="bi bi-plus-lg me-2"></i>Add car</a>
        <?php endif; ?>
    </div>
    <div class="row justify-content-center g-5">
        <form class="alert alert-secondary text-black bg-gradient <?= $isFiltered ? 'd-flex' : 'd-none' ?> flex-column col-10 col-lg-2 "
              id="filter-panel" method="get" action="">
            <input type="hidden" name="search" value="" id="search-hidden">
            <div class="mb-1">
                <i class="bi bi-filter me-2"></i><span>Sort by</span>
            </div>
            <div class="mb-4">
                <div>
                    <input type="radio" name="sortBy" id="sort-by-rating"
                           value="avg_rating"
                           class="form-check-input"
                        <?= $_GET["sortBy"] === 'avg_rating' ? 'checked' : '' ?>>
                    <label for="sort-by-rating" class="form-check-label">Rating</label>
                </div>
                <div>
                    <input type="radio" name="sortBy" id="sort-by-price"
                           value="price" class="form-check-input"
                        <?= $_GET["sortBy"] === 'price' ? 'checked' : '' ?>>
                    <label for="sort-by-price" class="form-check-label">Price</label>
                </div>

            </div>
            <div class="mb-1">
                <i class="bi bi-arrow-down-up me-2"></i><span>Sort type</span>
            </div>
            <div class="mb-4">
                <div>
                    <input type="radio" name="sortType" id="sort-type-ascending"
                           value="asc" class="form-check-input"
                        <?= $_GET["sortType"] === 'asc' ? 'checked' : '' ?>>
                    <label for="sort-type-ascending" class="form-check-label">Ascending</label>
                </div>
                <div>
                    <input type="radio" name="sortType" id="sort-type-descending"
                           value="desc" class="form-check-input"
                        <?= $_GET["sortType"] === 'desc' ? 'checked' : '' ?>>
                    <label for="sort-type-descending" class="form-check-label">Descending</label>
                </div>

            </div>
            <div class="mb-1">
                <i class="bi bi-tag-fill me-2 "></i><span>Car brand</span>
            </div>
            <div class="mb-4 filter-options-div">
                <?php foreach ($brands as $brand): ?>
                    <div>
                        <input type="checkbox" name="brands[]" id="brand-<?= $brand['id'] ?>"
                               value="<?= $brand['id'] ?>" class="form-check-input"
                            <?= in_array($brand['id'], $_GET["brands"] ?? []) ? 'checked' : '' ?>>
                        <label for="brand-<?= $brand['id'] ?>" class="form-check-label"><?= $brand['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="mb-1">
                <i class="bi bi-car-front-fill me-2"></i><span>Categories</span>
            </div>
            <div class="mb-4">
                <?php foreach ($categories as $category): ?>
                    <div>
                        <input type="checkbox" name="categories[]" id="category-<?= $category['id'] ?>"
                               value="<?= $category['id'] ?>" class="form-check-input"
                            <?= in_array($category['id'], $_GET["categories"] ?? []) ? 'checked' : '' ?>>
                        <label for="category-<?= $category['id'] ?>"
                               class="form-check-label"><?= $category['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="mb-1">
                <i class="bi bi-joystick me-2"></i><span>Transmission</span>
            </div>
            <div class="mb-4">
                <?php foreach ($transmissions as $transmission): ?>
                    <div>
                        <input type="checkbox" name="transmissions[]" id="transmission-<?= $transmission['id'] ?>"
                               value="<?= $transmission['id'] ?>" class="form-check-input"
                            <?= in_array($transmission['id'], $_GET["transmissions"] ?? []) ? 'checked' : '' ?>>
                        <label for="transmission-<?= $transmission['id'] ?>"
                               class="form-check-label"><?= $transmission['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="mb-1">
                <i class="bi bi-fuel-pump-fill me-2"></i><span>Fuel type</span>
            </div>
            <div class="mb-4">
                <?php foreach ($fuelTypes as $fuelType): ?>
                    <div>
                        <input type="checkbox" name="fuel_types[]" id="fuel-type-<?= $fuelType['id'] ?>"
                               value="<?= $fuelType['id'] ?>" class="form-check-input"
                            <?= in_array($fuelType['id'], $_GET["fuel_types"] ?? []) ? 'checked' : '' ?>>
                        <label for="fuel-type-<?= $fuelType['id'] ?>"
                               class="form-check-label"><?= $fuelType['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="mb-1">
                <i class="bi bi-geo-alt-fill me-2"></i><span>Cities</span>
            </div>
            <div class="mb-4 ">
                <?php foreach ($cities as $city): ?>
                    <div>
                        <input type="checkbox" name="cities[]" id="fuel-type-<?= $city['name'] ?>"
                               value="<?= $city['name'] ?>" class="form-check-input"
                            <?= in_array($city['name'], $_GET["cities"] ?? []) ? 'checked' : '' ?>>
                        <label for="fuel-type-<?= $city['name'] ?>"
                               class="form-check-label"><?= $city['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="mb-2">
                <i class="bi bi-cash-stack me-2"></i><span>Price</span>
            </div>
            <div class="mb-4">
                <input type="number" name="minPrice" id="minPrice" class="form-control mb-1" placeholder="Min price"
                       value="<?= $_GET["minPrice"] ?>">
                <input type="number" name="maxPrice" id="maxPrice" class="form-control " placeholder="Max price"
                       value="<?= $_GET["maxPrice"] ?>">
            </div>
            <div>
                <button class="btn btn-success w-100">Filter</button>
            </div>
        </form>

        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 g-4 col-12 col-lg-10 h-100">
            <?php foreach ($cars as $car): ?>
                <div class="col">
                    <a href="/cars/view/<?= $car['id'] ?>" class="card-link ">
                        <div class="card border-0 overflow-hidden shadow-sm car-card <?= $car['visible'] == 0 ? 'car-hidden' : '' ?>">
                            <?php if (empty($car['photo'])): ?>
                                <img src="/static/img/no-image.svg" class="card-img-top z-0 " alt="...">
                            <?php else: ?>
                                <img src="/files/car/<?= $car['photo'] ?>" class="card-img-top z-0 " alt="...">
                            <?php endif; ?>
                            <div class="card-body z-1 bg-white">
                                <h5 class="card-title text-truncate overflow-hidden"><?= $car['model'] ?></h5>
                                <div class="d-flex align-items-center rating">
                                    <i class="bi bi-star-fill text-primary d-flex align-items-center"></i>
                                    <span class="text-primary mt-1 mx-1 fw-medium "><?= $car['avg_rating'] !== NULL ? $car['avg_rating'] : 'No rating' ?></span>
                                </div>
                                <div class="d-flex mt-2 mb-1">
                                    <div class="d-flex city" style="opacity: .6">
                                        <i class="bi bi-geo-alt-fill me-2"></i>
                                        <span class="overflow-hidden text-truncate fw-medium "><?= $car['city'] ?></span>
                                    </div>
                                    <span class="text-end fw-bold w-100">$<?= $car['price'] ?><small
                                                class="fw-light text-secondary"> /day</small></span>
                                </div>
                                <?php if (User::isAdmin()): ?>
                                    <div class="py-3 d-flex justify-content-center gap-1">
                                        <a href="/cars/edit/<?= $car['id'] ?>" class="btn btn-primary">
                                            Edit</a>
                                        <a href="/cars/delete/<?= $car['id'] ?>" class="btn btn-danger">Delete</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script src="/static/js/filter.js"></script>