<?php
/** @var array $car */
\core\Core::getInstance()->pageParams['title'] = 'Delete Car';

?>
<div class="container ">
    <div class="row row-cols-8 row-cols-xl-4 justify-content-center">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Delete car?</h4>
            Car can't be restored in future.
            <hr>
            <p class="mb-0">
                <a href="/cars/delete/<?= $car['id'] ?>/yes" class="btn btn-danger">Delete</a>
                <a href="/cars/" class="btn btn-outline-danger">Cancel</a>
            </p>
        </div>
    </div>
</div>