<?php
/** @var array $car */
/** @var array $car_photos */
/** @var array $car_reviews */
/** @var array $car_trips_count */
/** @var array $car_brand */
/** @var array $car_category */
/** @var array $car_transmission */
/** @var array $car_fuel_type */
/** @var array $errors */
/** @var array $model */
/** @var array $user_rent */

\core\Core::getInstance()->pageParams['title'] = $car['model'];
$avg_rating = 0.0;
if (count($car_reviews) != 0) {
    foreach ($car_reviews as $review) {
        $avg_rating += floatval($review['rating']);
    }
    $avg_rating /= count($car_reviews);
    $avg_rating = round($avg_rating, 1);
}
?>
<style>
    .carousel .carousel-item img {
        height: 70vh;
    }

    .icon {
        width: 24px;
        height: 24px;
    }

    .car-properties {

    }

    .property-title span {
        font-size: 14px;
    }

    .car-properties span:not(.property-title > span) {
        font-size: 14px;
        font-weight: bold;
    }

    .property-title img {
        width: 20px;
        height: 20px;
    }

    .rate-stars {
        cursor: pointer;
    }

    .carousel-inner img {
        cursor: pointer;
    }

    .full-img {
        background: rgba(0, 0, 0, .9);
        justify-content: center;
        align-items: center;
    }

    @media screen and (max-width: 768px) {
        .carousel-inner img {
            height: 40vh !important;
        }
    }
</style>
<link rel="stylesheet" href="../../themes/light/css/mainStyle.css">
<link rel="stylesheet" href="../../themes/light/css/forms.css">
<link rel="stylesheet" href="../../themes/light/css/reviewStyle.css">
<div id="carouselExampleIndicators" class="carousel slide ">
    <div class="carousel-indicators">
        <?php for ($i = 0; $i < count($car_photos); $i++): ?>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="<?= $i ?>"
                <?php if ($i == 0): ?>
                    class="active"
                    aria-current="true"
                <?php endif; ?>
            ></button>
        <?php endfor; ?>
    </div>
    <div class="carousel-inner">
        <?php for ($i = 0; $i < count($car_photos); $i++): ?>
            <div class="carousel-item <?= $i == 0 ? 'active' : '' ?>">
                <img src="/files/car/<?= $car_photos[$i]['photo'] ?>" class="d-block w-100 object-fit-cover car-img"
                     alt="...">
            </div>
        <?php endfor; ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<div class="container">
    <div class="row mt-5">
        <div class="col-12 col-lg-8 mb-5">
            <div class="d-flex align-items-center gap-2">
                <a href="/cars/?brands%5B%5D=<?= $car['brand_id'] ?>">
                    <img src="/files/brand/<?= $car_brand['photo'] ?>" style="height: 50px; object-fit: contain" alt="">
                </a>
                <h1 class="mb-3 d-inline ms-1"><?= $car['model'] ?></h1>
            </div>
            <div class="d-flex align-items-center mt-3 rating-div">
                <i class="bi bi-star-fill text-primary d-flex align-items-cent  er"></i>
                <span class="text-primary mt-1 mx-2 fw-bolder rating-digit"><?= count($car_reviews) !=
                    0 ? number_format($avg_rating, 1, '.', '') : 'No rating' ?></span>
                <span class="mt-1">(<?= !empty($car_trips_count) ? $car_trips_count : '0' ?> trips)</span>
            </div>
            <div class="row row-cols-2 row-cols-md-3 row-gap-5 mt-4 car-properties">
                <?php if (!empty($car_category)): ?>
                    <div class="col d-flex justify-content-center flex-column gap-2 ">
                        <div class="d-flex gap-1 opacity-75 property-title">
                            <img src="/static/img/category-icon.png" alt="" class="icon">
                            <span>Category</span>
                        </div>
                        <span><?= $car_category['name'] ?></span>
                    </div>
                <?php endif; ?>
                <div class="col d-flex justify-content-center flex-column gap-2 ">
                    <div class="d-flex gap-1 opacity-75 property-title">
                        <img src="/static/img/kilometrage-icon.png" alt="" class="icon">
                        <span>Kilometrage</span>
                    </div>
                    <span><?= number_format($car['kilometrage'], 0, '', ' ') ?> km</span>
                </div>
                <?php if (!empty($car_transmission)): ?>
                    <div class="col d-flex justify-content-center flex-column gap-2 ">
                        <div class="d-flex gap-1 opacity-75 property-title">
                            <img src="/static/img/gear-stick-icon.png" alt="" class="icon">
                            <span>Transmission</span>
                        </div>
                        <span><?= $car_transmission['name'] ?></span>
                    </div>
                <?php endif; ?>
                <div class="col d-flex justify-content-center flex-column gap-2 ">
                    <div class="d-flex gap-1 opacity-75 property-title">
                        <img src="/static/img/seat-icon.png" alt="" class="icon">
                        <span>Seats</span>
                    </div>
                    <span><?= $car['seats'] ?></span>
                </div>
                <?php if (!empty($car_fuel_type)): ?>
                    <div class="col d-flex justify-content-center flex-column gap-2 ">
                        <div class="d-flex gap-1 opacity-75 property-title ">
                            <img src="/static/img/fuel-icon.png" alt="" class="icon">
                            <span>Fuel type</span>
                        </div>
                        <span><?= $car_fuel_type['name'] ?></span>
                    </div>
                <?php endif; ?>
                <div class="col d-flex justify-content-center flex-column gap-2 ">
                    <div class="d-flex gap-1 opacity-75 property-title">
                        <img src="/static/img/gas-pump-icon.png" alt="" class="icon">
                        <span>Consumption</span>
                    </div>
                    <span><?= $car['fuel_consumption'] ?> per 100 km</span>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4" id="side-block">
            <div>
                <h5 class="d-inline" id="price-text">$<?= floatval($car['price']) ?></h5>
                <span class="text-secondary" id="days-text">per day</span>
            </div>
            <hr>
            <?php if (\models\User::isUserAuthenticated()): ?>
                <?php if (!empty($user_rent)): ?>
                    <div class="mb-3">
                        <?php if ($user_rent['approved'] == 1): ?>
                            <div class="text-success">
                                <i class="bi bi-check-square-fill"></i>
                                <?php if ($user_rent['start_date'] <= date('Y-m-d') && $user_rent['end_date'] >= date('Y-m-d')): ?>
                                    <span>You are currently renting</span>
                                <?php else: ?>
                                    <span>Your rental request accepted</span>
                                <?php endif; ?>
                            </div>
                        <?php elseif ($user_rent['approved'] == 2): ?>
                            <div class="text-danger">
                                <i class="bi bi-x-square-fill"></i>
                                <span>Your last rental request declined</span>
                            </div>
                        <?php else: ?>
                            <div class="text-primary">
                                <i class="bi bi-hourglass-split"></i>
                                <span> Your rental request is in progress</span>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if ($user_rent['approved'] == 0): ?>
                        <a href="/cars/cancelRent/<?= $user_rent['id'] ?>" class="btn btn-danger w-100">Cancel</a>
                    <?php elseif ($user_rent['approved'] == 2): ?>
                        <a href="/cars/cancelRent/<?= $user_rent['id'] ?>" class="btn btn-danger w-100">Delete
                            request</a>
                    <?php elseif ($user_rent['approved'] == 1 && $user_rent['start_date'] > date('Y-m-d')) : ?>
                        <a href="/cars/cancelRent/<?= $user_rent['id'] ?>?redirect=/user/account/"
                           class="btn btn-danger w-100">Cancel</a>
                    <?php endif; ?>

                <?php else: ?>
                    <form action="" method="post" id="rent-form">
                        <input type="hidden" name="type" value="rent">
                        <div class="mb-3">
                            <label for="start_date" class="form-label">Trip start</label>
                            <input type="date" class="form-control" id="start_date" name="start_date"
                                   aria-describedby="startHelp" value="<?= $model['start_date'] ?>" required>
                            <div id="startHelp" class="form-text error-text"><?= $errors['start_date'] ?></div>

                        </div>
                        <div class="mb-3">
                            <label for="end_date" class="form-label">Trip end</label>
                            <input type="date" class="form-control" id="end_date" name="end_date"
                                   aria-describedby="endHelp" value="<?= $model['end_date'] ?>" required>
                            <div id="endHelp" class="form-text error-text"><?= $errors['end_date'] ?></div>
                        </div>
                        <div class="mb-3 ">
                            <i class="bi bi-geo-alt-fill"></i>
                            <span><?= $car['city'] ?></span>
                        </div>
                        <div class="mb-3 ">
                            <label for="delivery_location" class="form-label">Delivery location</label>
                            <input type="text" class="form-control" id="delivery_location" name="delivery_location"
                                   aria-describedby="locationHelp" value="<?= $model['delivery_location'] ?>" required>
                            <div id="locationHelp" class="form-text error-text"><?= $errors['location'] ?></div>
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Rent</button>
                        <span class="text-danger d-block mt-2 w-100 text-center" id="rent-error"></span>
                    </form>
                <?php endif; ?>
            <?php else: ?>
            <div class="d-flex flex-column gap-2 align-items-stretch">
                <span class="text-center">To rent car</span>
                <a href="/user/login/" type="button" class="btn btn-outline-dark me-2">Login</a>
                <span class="text-center">or</span>
                <a href="/user/register/" type="button" class="btn btn-success">Register</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-12 col-lg-10 mt-5">
            <h5>Description</h5>
            <p><?= $car['description'] ?></p>
        </div>
        <div class="col-12 col-lg-10 mt-5">
            <div class="mb-3">
                <h5 class="d-inline me-2">Reviews</h5>
                <span class="text-secondary" id="ratings-count">(<?= count($car_reviews) ?> ratings)</span>
            </div>
            <?php if (\models\User::isUserAuthenticated()): ?>

                <form action="" method="post" id="review-form" class="mb-5 col-12 col-md-6">
                    <label class="form-label">Your rate</label>
                    <div class="text-primary rate-stars">
                        <?php for ($i = 0; $i < 5; $i++): ?>
                            <i class="bi bi-star star" data-index="<?= $i ?>"></i>
                        <?php endfor; ?>
                    </div>
                    <label for="text" class="form-label mt-3">Text</label>
                    <textarea class="form-control" name="" id="text" cols="10" rows="5"
                              aria-describedby="reviewHelp"></textarea>
                    <div id="reviewHelp" class="form-text error-text review-error mb-3"></div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            <?php endif; ?>
            <div class="row justify-content-center align-items-center row-cols-1 row-gap-2 reviews-div">
                <?php foreach ($car_reviews as $review): ?>
                    <div class="col row author-id-<?= $review['user_id'] ?> review-<?= $review['id'] ?>">
                        <div class="col-10">
                            <div class="text-primary">
                                <?php for ($i = 0; $i < intval($review['rating']); $i++): ?>
                                    <i class="bi bi-star-fill"></i>
                                <?php endfor; ?>
                                <?php for ($i = 0; $i < 5 - intval($review['rating']); $i++): ?>
                                    <i class="bi bi-star"></i>
                                <?php endfor; ?>
                            </div>
                            <div class="mt-1 mb-2">
                                <span class="review-author "><?= $review['firstname'] . ' ' . $review['lastname'] ?></span>
                                <small class="review-date text-secondary"><?= date_format(date_create($review['date']), "d M Y ") ?></small>
                            </div>
                            <p class="review-text"><?= $review['text'] ?></p>
                        </div>
                        <div class="col-2 d-flex align-items-center justify-content-end">
                            <?php if (\models\User::isAdmin() || $review['user_id'] == \models\User::getCurrentAuthenticatedUser()['id']): ?>
                                <button class="btn btn-danger delete-review d-flex gap-2"
                                        data-review-id="<?= $review['id'] ?>">
                                    <i class="bi bi-trash-fill pe-none"></i>
                                    <span class="d-none d-md-block pe-none">Delete</span>
                                </button>
                            <?php endif; ?>
                        </div>
                        <hr>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="d-none" id="car-id"><?= $car['id'] ?></div>
</div>
<script src="/static/js/review.js"></script>
<script src="/static/js/rentCar.js"></script>
<script src="/static/js/viewFullPhoto.js"></script>