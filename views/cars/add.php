<?php
/** @var array $errors */
/** @var array $model */
/** @var array $brands */
/** @var array $categories */
/** @var array $transmissions */
/** @var array $fuelTypes */
\core\Core::getInstance()->pageParams['title'] = 'Add car';

?>
<style>
    .ck {
        height: 300px;
    }

    .car-photo {
        height: 200px;
    }

    .car-photos img {
        object-fit: cover;
    }
</style>
<link rel="stylesheet" href="../../themes/light/css/forms.css">
<div class="container">
    <div class="row row-cols-1   justify-content-center">
        <div class="col-10">
            <h2>New Car</h2>
            <form action="" method="post" enctype="multipart/form-data" class="row row-cols-1 row-cols-md-2">
                <div class="mb-3">
                    <label for="brand_id" class="form-label">Car brand</label>
                    <select name="brand_id" id="brand_id" class="form-control" aria-describedby="brandHelp" required>
                        <option value="0"></option>
                        <?php foreach ($brands as $brand): ?>
                            <option value="<?= $brand['id'] ?>" <?= $model['brand_id'] == $brand['id'] ? 'selected' : '' ?>><?= $brand['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div id="brandHelp" class="form-text error-text"><?= $errors['brand'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="category_id" class="form-label">Car category</label>
                    <select name="category_id" id="category_id" class="form-control" aria-describedby="categoryHelp">
                        <option value="0"></option>
                        <?php foreach ($categories as $category): ?>
                            <option value="<?= $category['id'] ?>" <?= $model['category_id'] == $category['id'] ? 'selected' : '' ?>><?= $category['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div id="categoryHelp" class="form-text error-text"><?= $errors['category_id'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="model" class="form-label">Car model</label>
                    <input type="text" class="form-control" id="model" name="model" aria-describedby="modelHelp"
                           value="<?= $model['model'] ?>" required>
                    <div id="modelHelp" class="form-text error-text"><?= $errors['model'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="price" class="form-label">Price ($ per day)</label>
                    <input type="number" class="form-control" id="price" name="price" aria-describedby="priceHelp"
                           value="<?= $model['price'] ?>" min="0" required>
                    <div id="priceHelp" class="form-text error-text"><?= $errors['price'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="kilometrage" class="form-label">Kilometrage</label>
                    <input type="number" class="form-control" id="kilometrage" name="kilometrage"
                           aria-describedby="kilometrageHelp"
                           value="<?= $model['kilometrage'] ?>" required>
                    <div id="kilometrageHelp" class="form-text error-text"><?= $errors['kilometrage'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="transmission_id" class="form-label">Transmission</label>
                    <select name="transmission_id" id="transmission_id" class="form-control"
                            aria-describedby="transmissionHelp" required>
                        <?php foreach ($transmissions as $transmission): ?>
                            <option value="<?= $transmission['id'] ?>" <?= $model['transmission_id'] == $transmission['id'] ? 'selected' : '' ?>><?= $transmission['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div id="transmissionHelp" class="form-text error-text"><?= $errors['transmission_id'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="fuel_type_id" class="form-label">Fuel Type</label>
                    <select name="fuel_type_id" id="fuel_type_id" class="form-control"
                            aria-describedby="fuelHelp">
                        <?php foreach ($fuelTypes as $fuelType): ?>
                            <option value="<?= $fuelType['id'] ?>" <?= $model['fuel_type_id'] == $fuelType['id'] ? 'selected' : '' ?>><?= $fuelType['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div id="fuelHelp" class="form-text error-text"><?= $errors['fuel_type_id'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="fuel_consumption" class="form-label">Fuel consumption</label>
                    <input type="number" class="form-control" id="fuel_consumption" name="fuel_consumption"
                           aria-describedby="consumptionHelp"
                           value="<?= $model['fuel_consumption'] ?>" step="0.1" required>
                    <div id="consumptionHelp" class="form-text error-text"><?= $errors['fuel_consumption'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="city" class="form-label">City</label>
                    <input type="text" class="form-control" id="city" name="city" aria-describedby="cityHelp"
                           value="<?= $model['city'] ?>" required>
                    <div id=cityHelp" class="form-text error-text"><?= $errors['city'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="seats" class="form-label">Seats count</label>
                    <input type="number" class="form-control" id="seats" name="seats" aria-describedby="seatsHelp"
                           value="<?= $model['seats'] ?>" required>
                    <div id="seatsHelp" class="form-text error-text"><?= $errors['seats'] ?></div>
                </div>
                <div class="mb-3 w-100">
                    <label for="photos" class="form-label">Car photos</label>
                    <input type="file" class="form-control" id="photos" name="photos[]" accept="image/jpeg"
                           aria-describedby="photosHelp" required multiple>
                    <div id="photosHelp" class="form-text error-text"><?= $errors['photos'] ?></div>
                </div>
                <div class="mb-5 w-100 row row-cols-1 row-cols-md-2 row-cols-lg-3 d-flex align-items-center car-photos row-gap-5">
                </div>
                <div class="mb-3 w-100">
                    <input class="form-check-input me-2" type="checkbox" name="visible"
                           id="visible" <?= !empty($model) ? !empty($model['visible']) ? 'checked' : '' : 'checked' ?>>
                    <label class="form-check-label" for="visible">Visible</label>
                </div>
                <div class="mb-3 w-100">
                    <label for="editor" class="form-label">Description</label>
                    <textarea name="description" id="editor"
                              aria-describedby="descriptionHelp"><?= $model['description'] ?></textarea>
                    <div id="descriptionHelp" class="form-text error-text"><?= $errors['description'] ?></div>
                </div>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
</script>
<script src="/static/js/addCar.js"></script>

