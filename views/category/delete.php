<?php
/** @var array $category */
\core\Core::getInstance()->pageParams['title'] = 'Delete Category';

?>
<div class="container ">
    <div class="row row-cols-8 row-cols-xl-4 justify-content-center">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Delete category?</h4>

            Cars of this category will remain uncategorized. Are you sure you want to delete the category?
            <hr>
            <p class="mb-0">
                <a href="/category/delete/<?= $category['id'] ?>/yes" class="btn btn-danger">Delete</a>
                <a href="/category/" class="btn btn-outline-danger">Cancel</a>
            </p>
        </div>
    </div>
</div>