<?php
/** @var array $categories */

use models\User;
\core\Core::getInstance()->pageParams['title'] = 'Categories';

?>

<link rel="stylesheet" href="../../themes/light/css/mainStyle.css">
<div class="container">
    <div class="my-5 d-flex align-items-center">
        <h2 class="d-inline me-3">Categories list</h2>
        <?php if (\models\User::isAdmin()): ?>
            <a href="/category/add" class="btn btn-success">
                <i class="bi bi-plus-lg me-1"></i>
                Add category</a>
        <?php endif; ?>
    </div>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-5 g-4">
        <?php foreach ($categories as $category): ?>
            <div class="col text-center ">
                <a href="/cars/?categories%5B%5D=<?= $category['id'] ?>" class="card-link">
                    <div class="card border-0 overflow-hidden">
                        <img src="/files/category/<?= $category['photo'] ?>" class="card-img-top z-0"
                             alt="...">
                        <div class="card-body z-1 bg-white">
                            <span class="card-text"><?= $category['name'] ?></span>
                            <?php if (User::isAdmin()): ?>
                                <div class="py-3">
                                    <a href="/category/edit/<?= $category['id'] ?>" class="btn btn-primary">
                                        <i class="bi bi-pencil-fill me-2"></i>Edit</a>
                                    <a href="/category/delete/<?= $category['id'] ?>" class="btn btn-danger">
                                        <i class="bi bi-trash-fill me-2"></i>Delete</a>
                                </div>
                            <?php endif; ?>

                        </div>
                        <?php if (User::isAdmin()): ?>
                            <div class="card-body z-1 bg-white">
                            </div>
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>