<?php
/** @var array $model */
/** @var array $errors */
\core\Core::getInstance()->pageParams['title'] = 'Add Category';

?>
<link rel="stylesheet" href="../../themes/light/css/forms.css">

<div class="container">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3  justify-content-center">
        <div class="col">
            <h2>New category</h2>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="name" class="form-label">Category Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= $model['name'] ?>"
                           aria-describedby="nameHelp">
                    <div id="nameHelp" class="form-text error-text"><?= $errors['name'] ?></div>
                </div>
                <div class="mb-3">
                    <label for="file" class="form-label">Category photo</label>
                    <input class="form-control" type="file" id="file" name="file" accept="image/jpeg"
                           aria-describedby="fileHelp">
                    <div id="fileHelp" class="form-text error-text"><?= $errors['file'] ?></div>
                </div>
                <div>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>