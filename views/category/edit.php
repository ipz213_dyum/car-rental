<?php
/** @var array $category */
/** @var array $model */
/** @var array $errors */
\core\Core::getInstance()->pageParams['title'] = 'Edit Category';

?>
<link rel="stylesheet" href="../../themes/light/css/forms.css">

<div class="container">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3  justify-content-center">
        <div class="col">
            <h2>Edit category</h2>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="name" class="form-label">Category Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= $category['name'] ?>"
                           required>
                    <?php if (!empty($errors['name'])): ?>
                        <div class="form-text text-danger"><?= $errors['name'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <?php
                    $filePath = 'files/category/' . $category['photo'];
                    if (is_file($filePath)): ?>
                        <img src="/<?= $filePath ?>" class="img-thumbnail w-100 " alt="">
                    <?php else: ?>
                        <img src="/static/img/no-image.svg" class="img-thumbnail" alt="">
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label for="file" class="form-label">Change photo</label>
                    <input class="form-control" type="file" id="file" name="file" accept="image/jpeg"
                           aria-describedby="fileHelp">
                    <div id="fileHelp" class="form-text error-text"><?= $errors['file'] ?></div>
                </div>
                <div>
                    <button class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>