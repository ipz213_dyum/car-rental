<?php
\core\Core::getInstance()->pageParams['title'] = 'Error 401';
?>

<div class="container text-center d-flex flex-column justify-content-center align-items-center mt-5">
    <img src="/static/img/401.png" class="w-50" alt="">
    <h5 class="mt-5">Unauthorized</h5>
</div>