<?php
\core\Core::getInstance()->pageParams['title'] = 'Error 403';
?>

<div class="container text-center d-flex flex-column justify-content-center align-items-center mt-5">
    <img src="/static/img/403.png" class="w-50" alt="">
    <h5 class="mt-5">Forbidden</h5>
</div>