<?php
/** @var array $brands */
/** @var array $categories */
/** @var array $reviews */
?>
<link rel="stylesheet" href="/themes/light/css/mainStyle.css">
<link rel="stylesheet" href="/themes/light/css/reviewStyle.css">
<div class="wallpaper d-flex justify-content-center align-items-center">
    <form action="" class="mt-5 p-2 rounded row col-8 col-lg-5 " method="get" id="search-form">
        <div class="col">
            <div class="form-floating" role="search">
                <input type="text" name="search" class="form-control border-0" id="search-input"
                       placeholder="Search a car" autocomplete="off">
                <label for="search-input">Find the car</label>
            </div>
        </div>

    </form>
</div>
<h1 class="text-center m-5">Find your drive</h1>
<div class="w-75 container mt-5">
    <div class="d-flex flex-row justify-content-between align-items-center mb-4">
        <h5 class="text-wrap">Browse by make</h5>
        <div>
            <a href="/brand/" class="btn btn-outline-dark">View all</a>
        </div>
    </div>
    <div class="row align-items-center justify-content-center row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-6 g-4">
        <?php foreach ($brands as $brand): ?>
            <div class="col text-center">
                <div class="card border-0">
                    <a href="/cars?brands%5B%5D=<?= $brand['id'] ?>" class="card-link">
                        <img src="/files/brand/<?= $brand['photo'] ?>" alt="<?= $brand['name'] ?>" class="brand">
                    </a>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="w-75 container mt-5">
    <div class="d-flex flex-row justify-content-between align-items-center mb-4 flex-wrap ">
        <h5 class="text-wrap">Browse by category</h5>
        <div>
            <a href="/category/" class="btn btn-outline-dark">View all</a>
        </div>
    </div>
    <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-4 g-4">
        <?php foreach ($categories as $category): ?>
            <div class="col text-center">
                <a href="/cars?categories%5B%5D=<?= $category['id'] ?>" class="card-link ">
                    <div class="card category-card border-0 overflow-hidden">
                        <img src="/files/category/<?= $category['photo'] ?>" class="card-img-top z-0" alt="...">
                        <div class="card-body z-1 bg-white">
                            <span class="card-title"><?= $category['name'] ?></span>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<h1 class="text-center m-5">Check out the reviews</h1>
<div class="w-75 container mt-5">
    <div class="d-flex flex-row justify-content-between align-items-center mb-4">
        <h5 class="text-wrap">Last reviews</h5>
    </div>

    <div class="row justify-content-center align-items-center row-cols-1 row-cols-lg-2 row-cols-xl-3 row-gap-4">
        <?php foreach ($reviews as $review): ?>
            <div class="col">
                <div class="py-3 px-4 card">
                    <div class="text-primary">
                        <?php for ($i = 0; $i < intval($review['rating']); $i++): ?>
                            <i class="bi bi-star-fill"></i>
                        <?php endfor; ?>
                        <?php for ($i = 0; $i < 5 - intval($review['rating']); $i++): ?>
                            <i class="bi bi-star"></i>
                        <?php endfor; ?>
                    </div>
                    <div class="mt-1 mb-2">
                        <span class="review-author "><?= $review['firstname'] . ' ' . $review['lastname'] ?></span>
                        <small class="review-date text-secondary"><?= date_format(date_create($review['date']), "d M Y ") ?></small>
                    </div>
                    <p class="review-text"><?= $review['text'] ?></p>
                    <div class="text-end">
                        <a href="/cars/view/<?= $review['car_id'] ?>">View <?= $review['model'] ?></a>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<script src="/static/js/mainPageSearch.js"></script>