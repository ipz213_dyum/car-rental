<?php
\core\Core::getInstance()->pageParams['title'] = 'Error 404';
?>

<div class="container text-center d-flex flex-column justify-content-center align-items-center mt-5">
    <img src="/static/img/404.png" class="w-50" alt="">
    <h5 class="mt-5">Not Found</h5>
</div>