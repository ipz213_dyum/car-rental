<?php
/** @var array $users */
/** @var array $filter */
\core\Core::getInstance()->pageParams['title'] = 'Users';

?>
<div class="container">
    <div class="d-flex align-items-center gap-3">
        <h2 class="d-inline">Users</h2>
        <form action="" method="get" id="select-type-form" class="d-flex  align-items-center gap-2">
            <input type="radio" class="btn-check" name="type" value="" id="all"
                   autocomplete="off" <?= $_GET['type'] == '' ? 'checked' : '' ?>>
            <label class="btn btn-secondary" for="all">All</label>
            <input type="radio" class="btn-check" name="type"
                   value="admins" id="admins"
                   autocomplete="off" <?= $_GET['type'] == 'admins' ? 'checked' : '' ?>>
            <label class="btn btn-secondary" for="admins">Admins</label>
            <input type="radio" class="btn-check" name="type"
                   value="managers" id="managers"
                   autocomplete="off" <?= $_GET['type'] == 'managers' ? 'checked' : '' ?>>
            <label class="btn btn-secondary" for="managers">Managers</label>
            <input type="text" id="filter-input" class="form-control " name="filter" value="<?= $filter ?>"
                   placeholder="Filter">
        </form>

    </div>
    <table class="table users-table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td>
                    <?= $user['login'] ?>
                    <?php if ($user['access_level'] == 10): ?>
                        <span class="badge rounded-pill text-bg-danger">Admin</span>
                    <?php endif; ?>
                    <?php if ($user['access_level'] == 5): ?>
                        <span class="badge rounded-pill text-bg-success">Manager</span>
                    <?php endif; ?>
                </td>
                <td><?= $user['phone_number'] ?></td>
                <td><?= $user['firstname'] ?></td>
                <td><?= $user['lastname'] ?></td>
                <td>
                    <input type="checkbox" name="" class="admin-checkbox"
                           id="admin-<?= $user['id'] ?>" <?= $user['access_level'] == 10 ? 'checked' : '' ?>
                           data-user-id="<?= $user['id'] ?>">
                    <label for="admin-<?= $user['id'] ?>">Admin</label>
                    <input type="checkbox" name="" class="manager-checkbox"
                           id="manager-<?= $user['id'] ?>" <?= $user['access_level'] == 5 ? 'checked' : '' ?>
                           data-user-id="<?= $user['id'] ?>">
                    <label for="manager-<?= $user['id'] ?>">Manager</label>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script src="/static/js/usersList.js"></script>