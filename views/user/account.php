<?php
/** @var array $user */
/** @var array $rents */
/** @var array $model */
/** @var array $errors */
if (empty($model)) {
    $model = $user;
}

\core\Core::getInstance()->pageParams['title'] = 'Account';

?>
<style>
    i::before {
        margin-bottom: 3px;
    }
</style>
<div class="container">
    <form action="" method="post" class="col">
        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <div class="mb-3 mt-5 d-flex gap-2">
                    <i class="bi bi-person-vcard-fill"></i>
                    <h5>Personal information</h5></div>
                <div class="row row-cols-1 row-cols-md-2 mb-3 row-gap-2">
                    <div class="col">
                        <label for="firstname" class="form-label">First name</label>
                        <input type="text" class="form-control" name="firstname" id="firstname"
                               value="<?= $model['firstname'] ?>"
                               aria-describedby="firstnameHelp"/>
                        <?php if (!empty($errors['firstname'])): ?>
                            <div id="firstnameHelp" class="form-text error-text"><?= $errors['firstname'] ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="col">
                        <label for="lastname" class="form-label">Last name</label>
                        <input type="text" class="form-control" name="lastname" id="lastname"
                               value="<?= $model['lastname'] ?>"
                               aria-describedby="lastnameHelp"/>
                        <?php if (!empty($errors['lastname'])): ?>
                            <div id="lastnameHelp" class="form-text error-text"><?= $errors['lastname'] ?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-md-2 mb-3 row-gap-2">
                    <div class="col">
                        <label for="login" class="form-label">Email</label>
                        <input type="email" class="form-control" name="login" id="login" value="<?= $model['login'] ?>"
                               aria-describedby="emailHelp"/>
                        <?php if (!empty($errors['login'])): ?>
                            <div id="emailHelp" class="form-text error-text"><?= $errors['login'] ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="col">
                        <label for="phone_number" class="form-label">Phone number</label>
                        <input type="tel" class="form-control" name="phone_number" id="phone_number"
                               value="<?= $model['phone_number'] ?>"
                               aria-describedby="numberHelp"/>
                        <?php if (!empty($errors['phone_number'])): ?>
                            <div id="numberHelp" class="form-text error-text"><?= $errors['phone_number'] ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="mb-3 mt-5 d-flex gap-2">
                    <i class="bi bi-key-fill"></i><h5>Change Password</h5></div>
                <div class="mb-3">
                    <label for="password-old" class="form-label">Old password</label>
                    <input type="password" class="form-control" name="password-old" id="password-old"
                           aria-describedby="passwordHelp"/>
                    <?php if (!empty($errors['password-old'])): ?>
                        <div id="passwordHelp" class="form-text error-text"><?= $errors['password-old'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label for="password-new" class="form-label">New password</label>
                    <input type="password" class="form-control" name="password-new" id="password-new"
                           aria-describedby="passwordRepeatHelp"/>
                    <?php if (!empty($errors['password-new'])): ?>
                        <div id="passwordRepeatHelp" class="form-text error-text"><?= $errors['password-new'] ?></div>
                    <?php endif; ?>
                </div>

            </div>
            <button type="submit" class="btn btn-primary w-25 mt-2 ms-2">Save</button>
        </div>

    </form>
    <hr class="mt-5">
    <div class="mt-5 mb-3 d-flex gap-2">
        <i class="bi bi-clock-history"></i><h5>Rent history</h5>
    </div>
    <div class="row row-cols-1 row-cols-lg-2 ">
        <?php foreach ($rents as $rent): ?>
            <div class="col">
                <div class="card mb-3 overflow-hidden ">
                    <div class="row g-0">
                        <div class="col-md-6 overflow-hidden">
                            <img src="/files/car/<?= $rent['photo'] ?>"
                                 class="w-100 h-100 object-fit-cover z-0" alt="...">
                        </div>
                        <div class="col-md-6 bg-white" style="height: 250px">
                            <div class="card-body z-1 ">
                                <div class="row">
                                    <a href="/cars/view/<?= $rent['car_id'] ?>" class="col-6">
                                        <h5 class="card-title text-nowrap text-truncate"><?= $rent['model'] ?></h5>
                                    </a>
                                    <span class="fw-bolder col-6 text-end">$<?= $rent['total_price'] ?>
                                        <small class="text-secondary ">(<?= $rent['days_count'] ?> days)</small></span>
                                </div>
                                <div class="row row-cols-1 my-3">

                                    <span class="card-text text-decoration-none ">Start date: <?= $rent['start_date'] ?></span>
                                    <span class="card-text text-decoration-none ">End date: <?= $rent['end_date'] ?></span>
                                </div>
                                <div><a href="/user/view/<?= $rent['user_id'] ?>">
                                        <?= $rent['firstname'] ?> <?= $rent['lastname'] ?>
                                    </a></div>
                                <div class="d-flex align-content-center gap-1 mt-2">
                                    <i class="bi bi-geo-alt-fill "></i>
                                    <span class=""><?= $rent['delivery_location'] ?></span>
                                </div>
                                <div>
                                    <?php if ($rent['approved'] == 1): ?>
                                        <div class="text-success d-flex align-content-center gap-1 mt-2">
                                            <i class="bi bi-check-square-fill"></i>
                                            <?php if ($rent['start_date'] <= date('Y-m-d') && $rent['end_date'] >= date('Y-m-d')): ?>
                                                <span>You are currently renting</span>
                                            <?php elseif ($rent['end_date'] < date('Y-m-d')): ?>
                                                <span>Rent finished</span>
                                            <?php else: ?>
                                                <span>Your rental request accepted</span>
                                            <?php endif; ?>
                                        </div>
                                    <?php elseif ($rent['approved'] == 2): ?>
                                        <div class="text-danger d-flex align-content-center gap-1 mt-2">
                                            <i class="bi bi-x-square-fill"></i>
                                            <span>Your last rental request declined</span>
                                        </div>
                                    <?php else: ?>
                                        <div class="text-primary d-flex align-content-center gap-1 mt-2">
                                            <i class="bi bi-hourglass-split"></i>
                                            <span> Your rental request is in progress</span>
                                        </div>
                                    <?php endif; ?>
                                    <div class="mt-2">
                                        <?php if ($rent['approved'] == 0): ?>
                                            <a href="/cars/cancelRent/<?= $rent['id'] ?>?redirect=/user/account/"
                                               class="btn btn-danger w-100">Cancel</a>
                                        <?php elseif ($rent['approved'] == 2): ?>
                                            <a href="/cars/cancelRent/<?= $rent['id'] ?>?redirect=/user/account/"
                                               class="btn btn-danger w-100">Delete
                                                request</a>
                                        <?php elseif ($rent['approved'] == 1 && $rent['start_date'] > date('Y-m-d')) : ?>
                                            <a href="/cars/cancelRent/<?= $rent['id'] ?>?redirect=/user/account/"
                                               class="btn btn-danger w-100">Cancel</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
