<?php
/** @var array $errors */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Register';
?>
<link rel="stylesheet" href="../../themes/light/css/forms.css">

<h1 class="h3 mb-3 fw-normal text-center">Log in</h1>
<main class="form-signin w-100 m-auto">
    <form action="" method="post">
        <div class="mb-3">
            <label for="login" class="form-label">Email</label>
            <input type="email" class="form-control" name="login" id="login" value="<?= $model['login'] ?>"
                   aria-describedby="emailHelp"/>
            <?php if (!empty($errors['login'])): ?>
                <div id="emailHelp" class="form-text error-text"><?= $errors['login'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="phone_number" class="form-label">Phone number</label>
            <input type="tel" class="form-control" name="phone_number" id="phone_number"
                   value="<?= $model['phone_number'] ?>"
                   aria-describedby="numberHelp"/>
            <?php if (!empty($errors['phone_number'])): ?>
                <div id="numberHelp" class="form-text error-text"><?= $errors['phone_number'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" name="password" id="password" value="<?= $model['password'] ?>"
                   aria-describedby="passwordHelp"/>
            <?php if (!empty($errors['password'])): ?>
                <div id="passwordHelp" class="form-text error-text"><?= $errors['password'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="password-repeat" class="form-label">Repeat password</label>
            <input type="password" class="form-control" name="password-repeat" id="password-repeat"
                   value="<?= $model['password-repeat'] ?>"
                   aria-describedby="passwordRepeatHelp"/>
            <?php if (!empty($errors['password-repeat'])): ?>
                <div id="passwordRepeatHelp" class="form-text error-text"><?= $errors['password-repeat'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="firstname" class="form-label">First name</label>
            <input type="text" class="form-control" name="firstname" id="firstname" value="<?= $model['firstname'] ?>"
                   aria-describedby="firstnameHelp"/>
            <?php if (!empty($errors['firstname'])): ?>
                <div id="firstnameHelp" class="form-text error-text"><?= $errors['firstname'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="lastname" class="form-label">Last name</label>
            <input type="text" class="form-control" name="lastname" id="lastname" value="<?= $model['lastname'] ?>"
                   aria-describedby="lastnameHelp"/>
            <?php if (!empty($errors['lastname'])): ?>
                <div id="lastnameHelp" class="form-text error-text"><?= $errors['lastname'] ?></div>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary w-100">Register</button>

    </form>
</main>


