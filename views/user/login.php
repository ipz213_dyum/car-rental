<?php
/** @var string|null $error */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Log in';
?>
<link rel="stylesheet" href="../../themes/light/css/forms.css">

<h1 class="h3 mb-3 fw-normal text-center">Log in</h1>
<main class="form-signin w-100 m-auto">
    <?php if (!empty($error)): ?>
        <div class="alert alert-danger" role="alert">
            <?= $error ?>
        </div>
    <?php endif; ?>
    <form method="post" action="" class="d-flex flex-column gap-3">
        <div class="form-floating">
            <input type="email" name="login" id="login" value="<?= $model['login'] ?>" class="form-control"
                   placeholder="name@example.com">
            <label for="login">Email address</label>
        </div>
        <div class="form-floating">
            <input type="password" name="password" id="password" value="<?= $model['password'] ?>" class="form-control"
                   placeholder="Password">
            <label for="password">Password</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Log in</button>
    </form>
</main>

