<?php
$months = ['All months', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
/** @var int $rentsCount */
/** @var int $moneyEarned */
/** @var float $avgRentPeriod */
/** @var array $rents */
/** @var array $model */
/** @var array $years */

\core\Core::getInstance()->pageParams['title'] = 'Statistics';

?>


<div class="container">
    <div class="row">
        <div class="col-9">
            <div class=" my-4">
                <h2>Dashboard</h2>
                <span class="text-secondary mb-5"> Here’s what’s going on at your business right now</span>
            </div>
            <section class="row my-5">
                <div class="col">
                    <div class="d-flex gap-3">
                        <img src="/static/img/money.png" style="width: 52px" alt="money">
                        <div>
                            <p class="h4 my-0">$<?= $moneyEarned ?></p>
                            <span class="text-secondary">total earned</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="d-flex gap-3">
                        <img src="/static/img/key.png" style="width: 52px" alt="money">
                        <div>
                            <p class="h4 my-0"><?= $rentsCount ?> cars</p>
                            <span class="text-secondary">were rented</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="d-flex gap-3">
                        <img src="/static/img/period.png" style="width: 52px" alt="money">
                        <div>
                            <p class="h4 my-0"><?= $avgRentPeriod ?> days</p>
                            <span class="text-secondary">avg rent period</span>
                        </div>
                    </div>
                </div>
            </section>
            <hr>
            <section class="row mb-4">
                <div class="col d-flex align-items-center gap-2">
                    <p class="h4 mb-0">Total rented</p>
                    <span class="badge bg-success fw-light px-3">
                        <?php if ($model['month'] == 0): ?>
                            in <?= $model['year'] ?>
                        <?php else: ?>
                            in <?= $months[$model['month']] ?>, <?= $model['year'] ?>
                        <?php endif; ?>
                    </span>
                </div>
                <div class="col">
                    <form id="requestStatisticForm" class="d-flex flex-row align-items-end gap-1" method="get"
                          action="">
                        <div class="mb-2">
                            <select name="month" class="form-select" id="monthSelect"
                                    aria-label="Default select example">
                                <?php for ($i = 0; $i < count($months); $i++): ?>
                                    <option
                                        value="<?= $i ?>" <?= $model['month'] == $i ? 'selected' : '' ?>><?= $months[$i] ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="mb-2">
                            <input name="year" type="number" value="<?= $model['year'] ?>" min="2000" step="1"
                                   class="form-control" id="yearFormControl" placeholder="Year">
                        </div>
                        <button type="submit" class="btn btn-primary flex-shrink-1 mb-2">Get report</button>
                    </form>
                </div>
            </section>

            <canvas id="rentsChart"></canvas>

        </div>
        <div class="col-3 pt-5">
            <div class="card p-4 mb-4">
                <span class="h6 mb-4">Rents by categories</span>

                <canvas id="categoriesChart"></canvas>

            </div>
            <div class="card p-4 mb-4">
                <span class="h6 mb-4">Rents by brands</span>

                <canvas id="brandsChart"></canvas>

            </div>
        </div>
        <section class="mt-4">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Car</th>
                    <th scope="col">User</th>
                    <th scope="col">Start date</th>
                    <th scope="col">End date</th>
                    <th scope="col">Price</th>
                    <th scope="col">Days</th>
                    <th scope="col">Total amount</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($rents); $i++): ?>
                    <tr>
                        <th scope="row"><?= $i + 1 ?></th>
                        <td><a href="/cars/view/<?= $rents[$i]['car_id'] ?>"> <?= $rents[$i]['model'] ?></a></td>
                        <td><?= $rents[$i]['firstname'] . ' ' . $rents[$i]['lastname'] ?></td>
                        <td><?= $rents[$i]['start_date'] ?></td>
                        <td><?= $rents[$i]['end_date'] ?></td>
                        <td>$<?= $rents[$i]['price'] ?></td>
                        <td><?= $rents[$i]['rentDays'] ?></td>
                        <td>$<?= $rents[$i]['totalAmount'] ?></td>
                    </tr>
                <?php endfor; ?>
                </tbody>
            </table>
        </section>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script type="module" src="../../dist/rentCharts.js"></script>