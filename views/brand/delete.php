<?php
/** @var array $brand */
\core\Core::getInstance()->pageParams['title'] = 'Delete Brand';

?>
<div class="container ">
    <div class="row row-cols-8 row-cols-xl-4 justify-content-center">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Delete brand?</h4>

            Cars of this brand will remain unbranded. Are you sure you want to delete the brand?
            <hr>
            <p class="mb-0">
                <a href="/brand/delete/<?= $brand['id'] ?>/yes" class="btn btn-danger">Delete</a>
                <a href="/brand/" class="btn btn-outline-danger">Cancel</a>
            </p>
        </div>
    </div>
</div>