<?php
/** @var array $errors */
/** @var array $model */
/** @var array $brand */
\core\Core::getInstance()->pageParams['title'] = 'Edit Brand';

if (empty($model))
    $model = $brand
?>
<link rel="stylesheet" href="../../themes/light/css/forms.css">
<div class="container">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3  justify-content-center">
        <div class="col-10">
            <h2>Edit Brand</h2>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="name" class="form-label">Brand Name</label>
                    <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp"
                           value="<?= $model['name'] ?>" required>
                    <div id="nameHelp" class="form-text error-text"><?= $errors['name'] ?></div>
                </div>
                <div class="mb-3">
                    <?php
                    $filePath = 'files/brand/' . $brand['photo'];
                    if (is_file($filePath)): ?>
                        <img src="/<?= $filePath ?>" class="img-thumbnail w-100" alt="">
                    <?php else: ?>
                        <img src="/static/img/no-image.svg" class="img-thumbnail" alt="">
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label for="photo" class="form-label">Change photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" accept="image/png"
                           aria-describedby="photoHelp">
                    <div id="photoHelp" class="form-text error-text"><?= $errors['photo'] ?></div>

                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
