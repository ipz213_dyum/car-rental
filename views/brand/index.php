<?php
/** @var array $brands */

use models\User;

\core\Core::getInstance()->pageParams['title'] = 'Brands';

?>
<link rel="stylesheet" href="../../themes/light/css/mainStyle.css">
<div class="container">
    <div class="my-5 d-flex align-items-center">
        <h2 class="d-inline me-3">Car brands list</h2>
        <?php if (\models\User::isAdmin()): ?>
            <a href="/brand/add/" class="btn btn-success">
                <i class="bi bi-plus-lg me-1"></i>
                Add brand</a>
        <?php endif; ?>
    </div>
    <div class="row row-cols-2 row-cols-lg-3 row-cols-xl-5 g-4">
        <?php foreach ($brands as $brand): ?>
            <div class="col text-center">
                <div class="card border-0">
                    <a href="/cars?brands%5B%5D=<?= $brand['id'] ?>" class="card-link">
                        <img src="/files/brand/<?= $brand['photo'] ?>" alt="<?= $brand['name'] ?>" class="brand">
                    </a>
                    <div class="card-body">
                        <span class="card-text"><?= $brand['name'] ?></span>
                    </div>
                    <?php if (User::isAdmin()): ?>
                        <div class="card-body">
                            <a href="/brand/edit/<?= $brand['id'] ?>" class="btn btn-primary mb-2 mb-md-0">
                                <i class="bi bi-pencil-fill me-2"></i>Edit</a>
                            <a href="/brand/delete/<?= $brand['id'] ?>" class="btn btn-danger">
                                <i class="bi bi-trash-fill me-2"></i>Delete</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

