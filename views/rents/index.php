<?php
/** @var array $rents */
/** @var array $requestsCount */
\core\Core::getInstance()->pageParams['title'] = 'Rents ';

?>
<style>
    .card-text {
        text-decoration: none;
    }

    p {
        font-size: 14px;
    }

    .status-text {
        font-size: 16px;
    }

    .card-body .card-text {
        font-weight: normal;
    }

    i::before {
        margin-bottom: 3px;
    }

</style>
<div class="container">
    <div class="d-flex flex-column flex-md-row align-items-lg-center gap-3">
        <h2>Rents</h2>
        <div>
            <form action="" method="get" id="select-type-form">
                <input type="radio" class="btn-check " name="type" value="" id="all"
                       autocomplete="off" <?= $_GET['type'] != 'requests' && $_GET['type'] != 'active' ? 'checked' : '' ?>>
                <label class="btn btn-secondary me-2 mb-2" for="all">All Rents</label>
                <input type="radio" class="btn-check" name="type"
                       value="requests" id="requests"
                       autocomplete="off" <?= $_GET['type'] == 'requests' ? 'checked' : '' ?>>
                <label class="btn btn-secondary position-relative me-2  mb-2" for="requests">Requests
                    <?php if ($requestsCount > 0): ?>
                        <span class="badge rounded-pill bg-danger position-absolute top-0 start-100 translate-middle">
                    <?= $requestsCount ?>
                    </span>
                    <?php endif; ?>
                </label>
                <input type="radio" class="btn-check" name="type" value="active" id="active"
                       autocomplete="off" <?= $_GET['type'] == 'active' ? 'checked' : '' ?>>
                <label class="btn btn-secondary me-2 mb-2 " for="active">Current rents</label>
                <input type="radio" class="btn-check " name="type" value="ended" id="ended"
                       autocomplete="off" <?= $_GET['type'] == 'ended' ? 'checked' : '' ?>>
                <label class="btn btn-secondary me-2 mb-2" for="ended">Ended rents</label>
            </form>
        </div>
    </div>
    <div class="row row-cols-1 row-cols-md-1 row-cols-lg-2 mt-5 row-gap-2">
        <?php foreach ($rents as $rent): ?>
            <div class="col">
                <div class="card mb-3 overflow-hidden">
                    <div class="row g-0">
                        <div class="col-md-6 overflow-hidden">
                            <img src="/files/car/<?= $rent['photo'] ?>"
                                 class="w-100 h-100 object-fit-cover z-0" alt="...">
                        </div>
                        <div class="col-md-6 bg-white" style="min-height: 300px">
                            <div class="card-body z-1 ">
                                <div class="row">
                                    <a href="/cars/view/<?= $rent['car_id'] ?>" class="col-6">
                                        <h5 class="card-title text-nowrap text-truncate"><?= $rent['model'] ?></h5>
                                    </a>
                                    <span class="fw-bolder col-6 text-end">$<?= $rent['total_price'] ?>
                                        <small class="text-secondary ">(<?= $rent['days_count'] ?> days)</small></span>
                                </div>
                                <div class="row row-cols-1 my-3">

                                    <span class="card-text text-decoration-none ">Start date: <?= $rent['start_date'] ?></span>
                                    <span class="card-text text-decoration-none ">End date: <?= $rent['end_date'] ?></span>
                                </div>
                                <div><a href="/user/index/?filter=<?= $rent['firstname'] ?>%20<?= $rent['lastname'] ?>">
                                        <?= $rent['firstname'] ?> <?= $rent['lastname'] ?>
                                    </a></div>
                                <div class="d-flex align-content-center gap-1 mt-2">
                                    <i class="bi bi-geo-alt-fill "></i>
                                    <span class=""><?= $rent['city'] ?>, <?= $rent['delivery_location'] ?></span>
                                </div>
                                <div class="d-flex align-content-center gap-1 mt-2">
                                    <i class="bi bi-phone "></i>
                                    <span class=""><?= $rent['phone_number'] ?></>
                                </div>
                                <div class="d-flex gap-3 mt-4 align-items-center">
                                    <?php if ($rent['start_date'] > date('Y-m-d') && $rent['end_date'] > date('Y-m-d')): ?>
                                        <?php if ($rent['approved'] == 1): ?>
                                            <span class="text-success status-text">Accepted</span>
                                            <a href="/rents/decline/<?= $rent['id'] ?>"
                                               class="btn btn-danger">Decline</a>
                                        <?php elseif ($rent['approved'] == 2): ?>
                                            <span class="text-danger status-text">Declined</span>
                                            <a href="/rents/accept/<?= $rent['id'] ?>"
                                               class="btn btn-success">Accept</a>
                                        <?php else: ?>
                                            <a href="/rents/accept/<?= $rent['id'] ?>?type=<?= $_GET["type"] ?>"
                                               class="btn btn-success">Accept</a>
                                            <a href="/rents/decline/<?= $rent['id'] ?>?type=<?= $_GET["type"] ?>"
                                               class="btn btn-danger">Decline</a>
                                        <?php endif; ?>
                                    <?php elseif ($rent['approved'] == 1 && $rent['start_date'] <= date('Y-m-d') && $rent['end_date'] >= date('Y-m-d')): ?>
                                        <span class="text-success status-text">In rent right now</span>
                                    <?php elseif ($rent['approved'] == 0): ?>
                                        <span class="text-danger status-text">Request expired</span>
                                        <a href="/rents/decline/<?= $rent['id'] ?>?type=<?= $_GET["type"] ?>"
                                           class="btn btn-danger">Delete</a>
                                    <?php else: ?>
                                        <span class="text-primary status-text">Rent ended</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<script>
    const selectTypeForm = document.body.querySelector('#select-type-form');
    selectTypeForm.addEventListener('click', (event) => {
        if (event.target.classList.contains('btn-check')) {
            selectTypeForm.submit();
        }
    });
</script>
